//######BIBLIOTECAS#####


//include<./../CASEgamma/CASEgamma.scad>;
//include<./../Componentes/boards.scad>;
include<./../Componentes/sensores.scad>;
include<./../Utilidades/Utilities.scad>;
include<./../Mecanica/OpenSCAD/Encoder.scad>;



//#####MISCELANEOS#####

//altura de technic 8mm
//media altura de technic 4mm

mediaAlturaTech=4;

//modulos con tolerancia negativa (sustra dimension de las medidas)
module mediaCruz(tol=0.1){
   
   //ancho de la barra en cruz
    espesorBTch=2.2;
   
    difference(){
       circle(d=5-(2*tol));
        translate([0,5+(espesorBTch/2)-tol])
       square([5*2,5*2],center=true);
        translate([0,-(5+(espesorBTch/2)-tol)])
       square([5*2,5*2],center=true);
    }
}




module techCruz(tol=0.1){

    //figura en vertical
    rotate(90)
    mediaCruz(tol=tol);
   
    //figura en horizontal
    mediaCruz(tol=tol);
}


module pinRedondo(tol=0.1,altura=mediaAlturaTech*2,topeInt=1,topeExt=1){

 diamExt=5.1;
 diamMed=4.63;
 diamInt=6;
  
   // translate([0,0,mediaAlturaTech])
 difference(){   
    union(){
       cylinder(d=diamMed-tol,h=altura);
        
        //tope interno
        if(topeInt==1){
        translate([0,0,1.5/4])
        cylinder(d=diamInt-tol,h=1.5/2,center=true);
        }
        
        //tope externo
        if(topeExt==1){
        translate([0,0,altura-(0.7/2)])
        cylinder(d=diamExt-tol,h=0.7,center=true);
        }
    }
    
    
    DimReMink=8;
    
    translate([0,0,altura])
   recortadorMink(DimReMink*2,0.81,DimReMink,0.8/2);
    
    rotate(90)
    // translate([0,0,(DimReMink)+(0.7/4)-11])
   recortadorMink(DimReMink*2,0.81,DimReMink,0.8/2);
}

}


module agujeroTech(tol=0.1,altura=mediaAlturaTech*2,topeInt=1,topeExt=1){


 diamMed=5.4;
 diamExt=6.2;
  
 union(){
       cylinder(d=diamMed+tol,h=altura);
        
     
        //tope externo A
      
        translate([0,0,1.5/4])
        cylinder(d=diamExt+tol,h=1.5/2,center=true);
        
        
        //tope externo B
       
        translate([0,0,altura-(1.5/4)])
        cylinder(d=diamExt+tol,h=1.5/2,center=true);
        
    }


}

module barrasTech(tol=0.1,tamaNio=4){



unidad=8;
    
    alturaBarra=7.4;

difference(){
    
    translate([-unidad/2,-(alturaBarra-(tol*2))/2,0])
    cube([unidad*tamaNio,(alturaBarra-(tol*2)),mediaAlturaTech*2]);
 
    
    
    for(i=[0:tamaNio]){
        translate([i*unidad,0,0])  
        agujeroTech();
    }
}

}

module topeCruz(tolEje=0.2,factor=1){
 
  difference(){
      cylinder(d=7.4,h=mediaAlturaTech*factor,$fn=100,center=true);
     translate([0,0,-mediaAlturaTech*factor])
        linear_extrude(height=mediaAlturaTech*2*factor)
      
        techCruz(tol=-tolEje);
      
      //solo se renderiza el tope para medios ejes*
      //*corregir
      if(factor==1){
        rotate_extrude()
        translate([(7.4/2)+0.3, 0])
           minkowski(){
               square([0.1,0.6],center=true);
               circle(d=1.5);
           }
       }
    }
    
   //echo("TopeCruzD");
   //echo()
   

}


//#####ENGRANES#####

module dienteEngrane20(tol=0.1){


ancho45A=1.24-(2*tol);
alto45A=1.56;
ancho45B=1.55-(2*tol);
alto45B=1.58;
sepTriangulosH=2.57;
sepTriangulosV=2.5;
   
mediaAlturaTech=4;
    baseAnclaje=1;


//se considera la tolerancia para la altura de los engranes
mediaCentro=mediaAlturaTech-sepTriangulosH-tol;



    
    translate([0,0,1.5])
    cube([ancho45B,2*mediaCentro,3],center=true);

hull(){

translate([0,sepTriangulosH+mediaCentro,0])
    hull(){
        rotate([90,0,0])
        linear_extrude(height=0.01)
        //circle(r=3,$fn=3);
        resize([ancho45A,alto45A])
        polygon(points=[[-1,0],[1,0],[0,1]]);

        translate([0,-sepTriangulosH,sepTriangulosV-0.01])
        
         linear_extrude(height=0.01)
        resize([ancho45B,alto45B])
         polygon(points=[[-1,0],[1,0],[0,1]]);
        //circle(r=4,$fn=);
    
    }
    
    rotate(180)
    translate([0,sepTriangulosH+mediaCentro,0])
    hull(){
        rotate([90,0,0])
        linear_extrude(height=0.01)
        //circle(r=3,$fn=3);
        resize([ancho45A,alto45A])
        polygon(points=[[-1,0],[1,0],[0,1]]);

        translate([0,-sepTriangulosH,sepTriangulosV-0.01])
        
         linear_extrude(height=0.01)
        resize([ancho45B,alto45B])
         polygon(points=[[-1,0],[1,0],[0,1]]);
        //circle(r=4,$fn=);
    
    }
    
}
    
       translate([0,0,-baseAnclaje/2])
cube([ancho45A,(mediaAlturaTech-tol)*2,baseAnclaje],center=true);

}//fin diente

module engraneP(tolEje=0.3,engrane=0,ajusteD=0.1){
//ajusteDiametral
//ajusteD=0.1;
    //medidas de engranajes (diametros)
//dia12T=7.68;
 dia12T=8.1;
dia20T=15.61;
    dia36T=31.5;
    tolH=0.1;


if(engrane==0){
    for(i=[0:11]){
  
        rotate((360/12)*i)
        translate([0,-(dia12T)/2-ajusteD,0])
        rotate([90,0,0])
         dienteEngrane20();
    }


    translate([0,0,-mediaAlturaTech+tolH])
    difference(){
      cylinder(d=dia12T+(ajusteD*2),h=(mediaAlturaTech-tolH)*2,$fn=100);
     translate([0,0,-mediaAlturaTech*2])
        linear_extrude(height=mediaAlturaTech*2*2)
        techCruz(tol=-tolEje);
    }
}

if(engrane==1){
    for(i=[0:19]){
        rotate((360/20)*i)
        translate([0,-dia20T/2,0])
        rotate([90,0,0])
         dienteEngrane20();
    }

    translate([0,0,-mediaAlturaTech+tolH])
    difference(){
      cylinder(d=dia20T+ajusteD,h=(mediaAlturaTech-tolH)*2,$fn=100);
     translate([0,0,-mediaAlturaTech*2])
        linear_extrude(height=mediaAlturaTech*2*2)
        techCruz(tol=-tolEje);
    }
}

if(engrane==2){
    for(i=[0:35]){
        rotate((360/36)*i)
        translate([0,-dia36T/2,0])
        rotate([90,0,0])
         dienteEngrane20();
    }


    translate([0,0,-mediaAlturaTech+tolH])
    difference(){
      cylinder(d=dia36T+ajusteD,h=(mediaAlturaTech-tolH)*2,$fn=100);
     translate([0,0,-mediaAlturaTech*2])
        linear_extrude(height=mediaAlturaTech*2*2)
        techCruz(tol=-tolEje);
    }engraneP(tolEje=0.3,engrane=1,ajusteD=0);
}




}

//cople para motor DC
module motorShaft(){
    // unidad=8;  
     shaftH=8;
     difference(){
     cylinder(d=7.4,h=shaftH);
     cylinder(d=2+2*(0.35),h=shaftH*2);
         }
     translate([0,0,-4])
     topeCruz(tolEje=0.15,factor=2);
}

//#######RENDERIZADOS########


//$fn=20;

//ejes technic

 /*
rotate([0,90,0])
rotate(45)
linear_extrude(height=mediaAlturaTech*2*4)
techCruz(0.1);
*/

//engraneP();


 //agujeroTech();

//barrasTech(tol=0.1,tamaNio=5);

//topeCruz(tolEje=0.3);


    

//pin 2 espacios
 
 /*
pinRedondo();
mirror([0,0,1]){
pinRedondo();
}
*/


//pin 3 espacios
/*
pinRedondo(tol=0.1,altura=mediaAlturaTech*2,topeInt=1,topeExt=1);
mirror([0,0,1]){
pinRedondo(tol=0.1,altura=mediaAlturaTech*2,topeInt=1,topeExt=0);
    translate([0,0,mediaAlturaTech*2])
    pinRedondo(tol=0.1,altura=mediaAlturaTech*2,topeInt=0,topeExt=1);
}
*/

//pin 1.5 espacios
/*
pinRedondo(tol=0.1,altura=mediaAlturaTech,topeInt=1,topeExt=0);
mirror([0,0,1]){
pinRedondo();
}
*/

//Engranes

//dienteEngrane20(tol=0.1);

//engraneP(tolEje=0.3,engrane=1,ajusteD=0);

//motorShaft();

/*
translate([0,0,-mediaAlturaTech])
engraneP(tolEje=0.3,engrane=0,ajusteD=0);
motorShaft();
*/
