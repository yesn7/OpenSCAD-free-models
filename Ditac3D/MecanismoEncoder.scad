include<./../Componentes/sensores.scad>;
include<./../Componentes/motores.scad>;
include<./../Utilidades/Utilities.scad>;
include<./../Mecanica/OpenSCAD/Encoder.scad>;
include<Technic.scad>;

//encoder Technic

module encoderTech(){
tol=0.1;
unidad=8;  
alturaBarra=7.4;
mediaAlturaTech=4;
taladroEntradas=9.3;

//radio del encoder son el diametro de eje technic con el tamaño de las barras del encoder
RadEnc=(10+(7.4/2));

ajuste=0.35;

translate([0,0,-mediaAlturaTech*(ajuste/2)])
linear_extrude(height=mediaAlturaTech*ajuste)
Encoder(cuentas=8,Radio=RadEnc,RadioEje=7.4/2,doble=0);
//se cambio de 0.2 a 0.15
topeCruz(tolEje=0.15,factor=ajuste);


}//fin encoder Tech
/*
translate([-RadEnc-0.5,0,0])
rotate([0,90,0])
sensorH21A1(modo=1,taladroEntradas=taladroEntradas,tornillo=2.5);
*/


module bracketH21A1Tech(){
tol=0.1;
unidad=8;  
alturaBarra=7.4;
mediaAlturaTech=4;
taladroEntradas=9.3;

//radio del encoder son el diametro de eje technic con el tamaño de las barras del encoder
RadEnc=(10+(7.4/2));

difference(){

union(){
    
difference(){
    //cubo que recibe pija
    translate([-RadEnc-0.5,-(alturaBarra-(tol*2))/2,1.61])
    cube([taladroEntradas+1.5,(alturaBarra-(tol*2)),(mediaAlturaTech*2)+2.75]);
    
    //sensor de herradura
   translate([-RadEnc-0.5,0,0])
rotate([0,90,0])
sensorH21A1(modo=1,taladroEntradas=9.3,tornillo=2.5,tol=1);   
}
 
//anclaje de barras technic
     translate([0,0,1.61])
    barrasTech(tol=0.1,tamaNio=2);

//extensión barra technic sin eje
 alturaBarra=7.4;
 translate([1.5*unidad,-(alturaBarra-(tol*2))/2,1.61])
cube([unidad,(alturaBarra-(tol*2)),unidad]);

//pieza sensor de herradura ESPEJO EN "Z"
mirror([0,0,1]){
   difference(){
    //cubo que recibe pija
    translate([-RadEnc-0.5,-(alturaBarra-(tol*2))/2,1.61])
    cube([taladroEntradas+1.5,(alturaBarra-(tol*2)),(mediaAlturaTech*2)+2.75]);
    
    //sensor de herradura
   translate([-RadEnc-0.5,0,0])
rotate([0,90,0])
sensorH21A1(modo=1,taladroEntradas=9.3,tornillo=2.5,tol=1);   
}
 
//anclaje de barras technic
     translate([0,0,1.61])
    barrasTech(tol=0.1,tamaNio=2); 

//extensión barra technic sin eje
// alturaBarra=7.4;
 translate([1.5*unidad,-(alturaBarra-(tol*2))/2,1.61])
cube([unidad,(alturaBarra-(tol*2)),unidad]);
    
}

//conector entre piezas
 translate([2*unidad,-(alturaBarra-(tol*2))/2,-unidad/2])
cube([unidad/2,(alturaBarra-(tol*2)),unidad]);

//salida a 90 grados para reduccion technic
translate([0,0,unidad/2+(tol*3.5)])
rotate([0,0,90])
     //difference(){
        barrasTech(tol=0.1,tamaNio=7);
         //translate([-unidad*1.5+(tol*4),-unidad/2,0])
        // cube([unidad,unidad,unidad]);
      //}
      
 //salida a 90 grados para reduccion technic complemento

    rotate([0,180,0])
    translate([0,0,unidad/2+(tol*3.5)])
    rotate([0,0,90])
         //difference(){
            barrasTech(tol=0.1,tamaNio=7);
           //  translate([-unidad*1.5+(tol*4),-unidad/2,0])
            // cube([unidad,unidad,unidad]);
          //}
 
 //union de 1 unidad
  rotate([0,180,0])
    translate([0,0,(unidad*1.5)+(tol*3.5)])
    rotate([0,0,90])
    barrasTech(tol=0.1,tamaNio=1);
    
     //barra de 4 unidades para reduccion
  rotate([0,180,0])
    translate([0,0,(unidad*2.5)+(tol*3.5)])
    rotate([0,0,90])
    barrasTech(tol=0.1,tamaNio=4);
      
    
 //cople para motorDC     
      translate([0,0,(unidad*3.5)+(tol)+1.71])
       rotate([180,0,0])
      //translate([0,0,-1.71])
      difference(){
    espesor=1.5;
    translate([0,0,1.71])
    rotate([180,0,0])
    cylinder(d=24+(2*espesor),h=10);
   motorDC3D_modA();  
}

//conexion con cople
translate([15.5-(unidad/2),-(unidad-(8*tol))/2,unidad+(4*tol)])
cube([unidad/2,unidad-(8*tol),(unidad*3.5)+1.71]);

//conexion con cople espejo
ajustePija=2.75;
mirror([1,0,0]){
    translate([14+(2*tol)-(unidad/2),-(unidad-(8*tol))/2,unidad+(4*tol)+ajustePija])
    cube([unidad/2,unidad-(8*tol),(unidad*3.5)+1.71-ajustePija]);
}


}//fin union

  translate([0,-(unidad*10)-(unidad/2)+(tol*4),0])
            cube([unidad*20,unidad*20,unidad*20],center=true);
         



}//fin difference

 }//fin bracket
 
 
    
 
 
 //####RENDERIZADOS####
 
$fn=200;

//motorShaft();

//engraneP(tolEje=0.3,engrane=1,ajusteD=0);

 //encoderTech();
 
// bracketH21A1Tech();
 
 
translate([0,0,-mediaAlturaTech])
engraneP(tolEje=0.3,engrane=0,ajusteD=0);
motorShaft();

 




//barrasTech(tol=0.1,tamaNio=2);