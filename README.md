
#Introduction/Introducción

## OpenSCAD

OpenSCAD is a software for creating 3D solid CAD. This is free software and is available for Linux/UNIX, Windows and Mac OS X. Unlike most 3D modeling software (such as BLender) it does not focus on the artistic aspects of 3D modeling but instead on the aspects of CAD. This way it may be the application you're looking for when you're planning 3D models of mechanical parts but maybe it's not what you're looking for if you're interested in creating models for computer animated movies.

OpenSCAD is not an interactive modeler. Instead it's something like a 3D compiler that reads a code file that describes the object and projects the 3D model from this code. This gives you (the designer) full control over the modeling process and allows you to easily change any step of the modeling process or make designs that are defined with configurable parameters.

OpenSCAD provides two main forms of modeling: First there is a constructive way of solid geometry (aka CSG) and the second is perimeter extrusion. The DXF extension is used as a 2D line data exchange format for use in AutoCAD. In addition to 2D trajectories for extrusion it is also possible to read design parameters from parameters of files with DXF extension. OpenSCAD can also read and create files with STL and OFF extensions.

Translated with www.DeepL.com/Translator

OpenSCAD es un software para crear CAD de sólidos en 3D. Éste es software libre y está disponible para Linux/UNIX, Windows y Mac OS X. A diferenecia de la mayoría del software para crear modelos 3D (como BLender) éste no se enfoca en los aspectos artísticos del modelado 3D pero en cambio en los aspectos del CAD. De éste modo puede ser la aplicación que esas buscando cuando estás planeando modelos 3D de partes mecánicas pero tal vez no es lo que estás buscando si estás interesado en crear modelos para películas animadas por computadora.

OpenSCAD no es un modelador interactivo. En cambio es algo como un compilador 3D que lee un archivo de código que describe el objeto y proyecta el modelo en 3D desde éste código. Ésto te da (al diseñador) control total sobre el proceso de modelaje y activa que cambies de forma sencilla cualquier paso del proceso de modelaje o hacer diseños que estén definidos con parámetros configurables.

OpenSCAD provee dos formas principales de modelado: Primero existe una manera constructica de geometría de sólidos (alias CSG) y el segundo está la extrusión perimetral. Se usa la extensión DXF como formato de intercambio de datos de líneas en 2D para en uso en AutoCAD. En complemento a las trayectorias 2D para extrusión es también posible leer los parámetros de diseño desde parámeros de archivos con extensión DXF. También OpenSCAD puede leer y crear archivos con extensiones STL y OFF.

## OpenSCAD-free-models

Éste proyecto es una colección de scripts parametrizables para la elaboración de proyectos 3D y 2D en lenguaje de OpenSCAD.

## Goal/Meta

This project is a collection of parameterizable scripts for the development of 3D and 2D projects in OpenSCAD language.

El siguiente repositorio tiene como objetivo ser una coleccion de srcirpts parametrizables que sirvan de uso cotidiano y ejemplo que puedan estar disponibles para todo público, desde el mantenimiento del hogar, piezas estéticas y piezas de ingeniería

# How to contribute/Como contribuir

The purpose of making a repository of OpenSCAD codes of parametrizable code is to increase the experience of use and to be able to realize 3D models dina'mica and with practical applications.

To be able to contribute it is necessary to have a gitlab account to be able to make a fork to this repository and later the changes will be reviewed by the administrators and later integrated or returned for review.

Each project must be contained in a folder and it is recommended that in each project there are subfolders called OpenSCAD, PNG, 3Dprint, STL and DXF (in some cases), in the case of the last two folders that have examples of dxf and stl, in addition to it is included a README.md file describing the function and / or reason for the project in question.

El propósito de hacer un repositorio de códigos de OpenSCAD de código parametrizable es aumentar la experiencia de uso y poder realizar modelos 3D dina'mica y con aplicaciones prácticas

Para poder contribuir es necesario tener una cuenta de gitlab para poder realizar un fork a éste repositorio y posteriormente los cambios serán revisados por los administradores y posteriormente integrados o regresados para revisión

Cada proyecto debe estar contenido en una carpeta y se recomienda que en cada proyecto existan subcarpetas llamadas OpenSCAD, PNG, 3Dprint,STL y DXF (en algunos casos), en el caso de las dos últimas carpetas que tengan ejemplos de dxf y stl, además de ello que se inluya un archivo README.md en el cual se describa la función y/o motivo del proyecto en cuestión.

# Code of Conduct/Código de Conducta

In designs and or 2D and 3D models must not contain inappropriate content or bad taste, this is to contain swear words or models with explicit content or inappropriate for sensitive audiences.

En los diseños y o modelos 2D y 3D no debe contener contenido inapropiado o de mal gusto, esto es contener groserías palabras antisonantes o modelos con contenido explícito o inadecuado para audiencias sensibles.

# Otros

visita los proyectos:

OpenSCAD
www.openscad.org

Idea 1.61
www.idea161.org

Piezas modulares para prototipos experimentales
www.facebook.com/DitacOficial

Servicio de Impresión 3D
www.facebook.com/idea161
