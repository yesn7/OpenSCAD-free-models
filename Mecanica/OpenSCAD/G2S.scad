module tornillo(){
    
    translate([0,9.33/2])
    union(){
    square([3.22,9.33],center=true);
    
    square([6.44,2.22],center=true);
    }
}

linear_extrude(height=8)
union(){
difference(){
square([52,18]);
  translate([9,0])
  tornillo();  
}

//anclaje
translate([30,-8])
square([15,8]);

//correccion (parche)
 translate([52,0])
square([10,5]);


//pieza rotada
angulo=15;

translate([62,0])
rotate(-angulo)
translate([-18,0])

difference(){
union(){
    square([18,25]);
    //anclaje
    translate([18,25-8])
    square([8,8]);
}
 rotate(-90)
    translate([-8,9.33])
    tornillo();     
}
}