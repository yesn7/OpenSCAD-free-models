use <MCAD/involute_gears.scad>


module recta(diametro=6,espesor=1.5,largo=60){

translate([0,-diametro/2-espesor,0])
rotate([180,0,90])
difference(){
   cube([diametro+(espesor*2),largo,diametro/2+espesor]);
    translate([diametro/2+espesor,0,0])
    rotate([-90,0,0])
    cylinder(d=diametro,h=largo);
}


}//fin recta

module curva(radioGiro=20,altura=5,diametro=6,divisiones=7,espesor=1.5,giro=45){

lonCubo=diametro+(2*espesor);

difference(){

//base de cubos
for(i=[0:(divisiones-1)]){
        rotate(i*(giro/divisiones))
        translate([0,0,(altura/divisiones)*i])
        hull(){
            translate([radioGiro, 0, 0])
            //sphere(d=diametro);
            translate([0,0,-espesor*2])
            cube([lonCubo,lonCubo,diametro-(2*espesor)],center=true);

            rotate(giro/divisiones)
            translate([radioGiro, 0, altura/divisiones])
            //sphere(d=diametro);
            translate([0,0,-espesor*2])
            cube([lonCubo,lonCubo,diametro-(2*espesor)],center=true);
        }
    }


//canaleta Esferas
    for(i=[0:(divisiones-1)]){
        rotate(i*(giro/divisiones))
        translate([0,0,(altura/divisiones)*i])
        hull(){
            translate([radioGiro, 0, 0])
            sphere(d=diametro);

            rotate(giro/divisiones)
            translate([radioGiro, 0, altura/divisiones])
            sphere(d=diametro);
        }
    }
    
    //canaleta Cilindros
    for(i=[0:(divisiones-1)]){
        rotate(i*(giro/divisiones))
        translate([0,0,(altura/divisiones)*i])
        hull(){
            translate([radioGiro, 0, 0])
            cylinder(d=diametro,h=lonCubo);
            //sphere(d=diametro);

            rotate(giro/divisiones)
            translate([radioGiro, 0, altura/divisiones])
            cylinder(d=diametro,h=lonCubo);
            //sphere(d=diametro);
        }
    }
    
    //recortador de inicio de angulo
    translate([0,-radioGiro*2,-radioGiro*2])
    cube([radioGiro*2,radioGiro*2,radioGiro*4]);
    
    rotate(giro-90)
     translate([-radioGiro*2,0,-radioGiro*2])
    cube([radioGiro*2,radioGiro*2,radioGiro*4]);

}


}//fin curva

//####RENDERIZADOS####

$fn=10;
//Alto=70;
diametro=6;
espesor=1.5;
largo=80;
pendiente=10;
//radio de giro minimo para el espesor 5mm
espesorEngrane=4;
tol=1;
radioGiro=(2*tol)+diametro;

alturaCurva=4;
baseRecta=cos(pendiente)*largo;
alturaRecta=sin(pendiente)*largo;

niveles=1;

//rectas1
translate([tol/2,0,0])
difference(){
    for(i=[0:niveles]){
      
    //translate([0,(radioGiro*niveles*2)-(radioGiro*i),2*(alturaRecta+alturaCurva)*i])
        translate([0,radioGiro,2*(alturaRecta+alturaCurva)*i])
    rotate([0,-pendiente,180])
    recta(diametro=diametro,espesor=espesor,largo=largo+tol);
        
    }

translate([-baseRecta/2,0,-alturaRecta/2])
cube([baseRecta,baseRecta,alturaRecta]);
}

//rectas2
translate([-baseRecta-tol/2,0,0])
rotate(180)
difference(){
    for(i=[0:niveles]){
      
    //translate([0,(radioGiro*niveles*2)-(radioGiro*i),(alturaRecta+alturaCurva)+2*(alturaRecta+alturaCurva)*i])
        translate([0,radioGiro,(alturaRecta+alturaCurva)+2*(alturaRecta+alturaCurva)*i])
    rotate([0,-pendiente,180])
    recta(diametro=diametro,espesor=espesor,largo=largo+tol);
        
    }

//corte de rectas
translate([-baseRecta-baseRecta/2,0,(alturaRecta+alturaCurva)*3])
cube([baseRecta,baseRecta,alturaRecta*2]);

}



//curvas1
translate([0,0,-alturaCurva])
for(i=[1:niveles]){
translate([0,0,2*(alturaRecta+alturaCurva)*i])
rotate(-90)
curva(radioGiro=radioGiro,altura=alturaCurva,diametro=6,divisiones=16,espesor=1,giro=180);
}

//curvas2
translate([-baseRecta,0,alturaRecta])
for(i=[0:niveles]){
translate([0,0,2*(alturaRecta+alturaCurva)*i])
rotate(90)
curva(radioGiro=radioGiro,altura=alturaCurva,diametro=6,divisiones=16,espesor=1,giro=180);
}

lonSoporte=4*(alturaRecta+alturaCurva);
anchoSoporte=2*(radioGiro-(diametro/2));


difference(){
    translate([0,0,-(diametro/2+espesor)])
   union(){
      cylinder(r=anchoSoporte/2,h=lonSoporte);

      translate([-baseRecta,0,0])
      cylinder(r=anchoSoporte/2,h=lonSoporte);

      translate([-baseRecta,-anchoSoporte/2,0])
      cube([baseRecta,anchoSoporte,lonSoporte]);
  }
  
  translate([-baseRecta/2,0,0])
  cube([baseRecta,anchoSoporte-(espesor*2),lonSoporte*2],center=true);
  
  }





  translate([-baseRecta/2,diametro/2,lonSoporte/2])
  rotate([90,0,0])
  gear(circular_pitch = 500,
		number_of_teeth = 27,
		gear_thickness = diametro,
		rim_thickness = diametro,
		hub_thickness = espesorEngrane,
		bore_diameter = espesorEngrane);



//rotate([90-20,0,90])
/*union(){
translate([-37.5,0,0])
gear(circular_pitch = 500,
		number_of_teeth = 27,
		gear_thickness = espesorEngrane,
		rim_thickness = espesorEngrane,
		hub_thickness = espesorEngrane,
		bore_diameter = espesorEngrane);


translate([9.72,0,0])
gear(circular_pitch = 500,
		number_of_teeth = 7,
		gear_thickness = espesorEngrane,
		rim_thickness = espesorEngrane,
		hub_thickness = espesorEngrane,
		bore_diameter = espesorEngrane);
}*/


/*
//recta 0
translate([0,-radioGiro,Alto])
rotate([0,pendiente,0])
recta(diametro=diametro,espesor=espesor,largo=largo+tol);

//prueba cubo
//translate([baseRecta,0,Alto-alturaRecta])
//cube([1,1,1]);


//curva0
translate([baseRecta,0,Alto-alturaRecta])
rotate(-90)
curva(radioGiro=radioGiro,altura=-alturaCurva,diametro=6,divisiones=16,espesor=1,giro=180);

//recta1
translate([baseRecta,radioGiro,Alto-alturaRecta-alturaCurva])
rotate([0,pendiente,180])
recta(diametro=diametro,espesor=espesor,largo=largo+tol);

rotate(90)
translate([0,0,Alto-(2*alturaRecta)-alturaCurva])
curva(radioGiro=radioGiro,altura=-alturaCurva,diametro=6,divisiones=16,espesor=1,giro=180);
*/

