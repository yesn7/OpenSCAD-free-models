

include<./../CASEgamma/CASEgamma.scad>;
include<./../Componentes/boards.scad>;
include<./../Componentes/sensores.scad>;
include<./../Utilidades/Utilities.scad>;

   
    
 


 module pastel(radio=30,curva=5,altura=10){
    difference(){
        minkowski(){
            cylinder(r=radio,h=altura*2,center=true);
            sphere(r=curva);
        }
        translate([0,0,-radio*1.5])
        cube(radio*3,radio*3,radio*3,center=true);
    }
}

module vela(altura=15,radio=2){

cylinder(h=altura,r=radio);
    translate([0,0,altura])
    hull(){
          translate([0,0,radio*4])
          sphere(r=0.1);
         sphere(r=1.5*radio);
    }
}

 //#######RENDERIZADOS########
  angulo=360/8;
$fn=100;

for(j=[0:2]){
    for(i=[0:7]){
      rotate(angulo*i+20*j) 
    translate([30-8*j,0,12+12*j])
        scale(0.5)
    vela();
    }
}

for(i=[0:2]){
    translate([0,0,12*i])
pastel(radio=30-8*i,curva=5,altura=8);
}


translate([-17,-26,1])
rotate([90,0,0])
linear_extrude(height=10)    
text(size=10,font = "Simplex","Vero");


