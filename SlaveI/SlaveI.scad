

//home/pablovc/Documentos/OpenSCAD-free-models/SistemaTiDi
include<./../CASEgamma/CASEgamma.scad>;
include<./../Componentes/boards.scad>;
include<./../Componentes/sensores.scad>;
include<./../Componentes/motores.scad>;
include<./../Utilidades/Utilities.scad>;

$fn=200;


module BaseSlave(medidaX=40,medidaY=40,medidaZ=40,medidaEstandar=40){

difference(){
resize([medidaX,medidaY,medidaZ])
sphere(r=medidaEstandar);
    //cubo abajo
    translate([-medidaEstandar,-medidaEstandar,-medidaEstandar*2])
    cube([medidaEstandar*2,2*medidaEstandar,medidaEstandar*2]);
    
     //cubo mitad
    translate([-medidaEstandar,-medidaEstandar*2,-medidaEstandar])
    cube([medidaEstandar*2,medidaEstandar*2,medidaEstandar*2]);
}

}




difference(){
    
union(){
    
difference(){
    union(){
rotate(180)
BaseSlave(medidaX=40,medidaY=40,medidaZ=40/3);

BaseSlave(medidaX=40,medidaY=40*1.75,medidaZ=40/3);
    }
    
    scale(0.9)
    union(){
    
rotate(180)
BaseSlave(medidaX=40,medidaY=40,medidaZ=40/3);

BaseSlave(medidaX=40,medidaY=40*1.75,medidaZ=40/3);

        }
}
//hombro1
translate([-5,0,5])
rotate([0,0,90])
BaseSlave(medidaX=20,medidaY=20*0.75,medidaZ=20);

//hombro2
mirror([1,0,0]){
  translate([-5,0,5])
   rotate([0,0,90])
  BaseSlave(medidaX=20,medidaY=20*0.75,medidaZ=20);
}

}//fin union hombros y base


//recorte nicho ala 1
translate([-10,0,0])
rotate([90,0,-90])
linear_extrude(height=10,scale=0.5)
square([15,40],center=true);

//recorte nicho ala 2
translate([10,0,0])
rotate([90,0,90])
linear_extrude(height=10,scale=0.5)
square([15,40],center=true);

}//fin difference recortadores



//CABINA
translate([0,0,4.5])
rotate([0,0,180])
BaseSlave(medidaX=15,medidaY=30,medidaZ=31);


//seccion baston
translate([0,0,7.5])
//recortador vertical
difference(){
resize([15,70,15])
rotate([-90+30,0,0])
    //recortador mitad
difference(){
cylinder(r1=40,r2=10,h=70);
translate([-40,0,-70])    
cube([80,80,140]);
}

//recorte ajuste de cabina
translate([-40,-80,-70])    
cube([80,80,140]);

//recorte Final
translate([-40,40,-70])    
cube([80,80,140]);
}

//Puerta abajo
translate([0,5,8])
 cube([12,10,5],center=true);


//Correccion angulo seccion baston
/*
Esta pieza es un cubo que atraviesa toda la escultura y le ayuda a tener estabilidad estructural en la impresion sin soporte
*/

union(){
difference(){
        
    translate([0,22,10.5])
    rotate([30,0,0])
    cube([6,35,7],center=true);
    
    //recortador abajo
    translate([0,0,-7])
    cube([6*2,35*2,7*2],center=true);
    
    //recortador arriba
     translate([0,5,3.5*7])
    cube([6*2,35*2,7*2],center=true);
    
        //recortador atras
     translate([0,-35+8,7])
    cube([6*2,35*2,7*2],center=true);
}

//parte secundaria sostenedor
 translate([0,12,10.5])
    cube([6,20,7],center=true);
}

//armas

translate([0,33,18])
union(){
    //eje apoyo
   rotate([0,90,0])
   cylinder(r=1,h=10,center=true);
    
    //arma1
    translate([5,0,2])
     cylinder(r=1,h=10,center=true);

    //arma2
    translate([-5,0,2])
     cylinder(r=1,h=10,center=true);
}


//docking ring
translate([0,-9,2.5])

difference(){
    union(){
     rondana (ancho=5,radioInterno=6,radioExterno=7);
    
    //disco interno
    rondana (ancho=5,radioInterno=3,radioExterno=4);
    
    }
    //recorte para acabado en "shell"
    translate([0,-13,9])
    rotate([20,0,0])
   cube([20,20,20],center=true);
}


translate([0,2,0])
linear_extrude(height=6)
difference(){
    radioTHext=2;
    radioTHint=1;
    separacionTh=4;
hull(){
    translate([separacionTh,0])
   circle(r=radioTHext);
  
    translate([-separacionTh,0])
    circle(r=radioTHext);
}

hull(){
    translate([separacionTh,0])
   circle(r=radioTHint);
  
    translate([-separacionTh,0])
    circle(r=radioTHint);
}


}

//thruster 1
translate([5,10,3])
union(){
 rondana (ancho=6,radioInterno=2,radioExterno=3);
    translate([4,0,-0.5])
cube([3,1,5],center=true);
}

//thruster 2

translate([-5,10,3])
union(){
 rondana (ancho=6,radioInterno=2,radioExterno=3);
    
     translate([-4,0,-0.5])
cube([3,1,5],center=true);
}

//eje alas
translate([0,0,4])
rotate([0,90,0])
   cylinder(r=1,h=50,center=true);

//eje ala 1

translate([-10,0,10])
rotate([0,60,0])
   cylinder(r=1,h=23,center=true);

//soporte vertical ala1

translate([-13,0,0])
//rotate([0,60,0])
   cylinder(r=1,h=8);



//eje ala 2

translate([10,0,10])
rotate([0,-60,0])
   cylinder(r=1,h=23,center=true);
   
   //soporte vertical ala2

translate([13,0,0])
//rotate([0,60,0])
   cylinder(r=1,h=8);




//ala 1
translate([27,0.5,6])
rotate([90,0,0])
linear_extrude(height=1)
difference(){
  square([12,12],center=true);
   difference(){
     square([6,6]);
    circle(r=6);
  }
}

//ala2
mirror([1,0,0]){
    translate([27,0.5,6])
rotate([90,0,0])
linear_extrude(height=1)
difference(){
  square([12,12],center=true);
   difference(){
     square([6,6]);
    circle(r=6);
  }
}
    
}

translate([-0.5,10,0])
rotate([90,0,0])
rotate([0,90,0])
linear_extrude(height=1)
polygon([[0,0],[20,0],[20,3],[0,6]]);

/*


BaseSlave();
BaseSlave(ancho=40,factorX=1,factorZ=40/3,factor=1);

BaseSlave(ancho=40,factorX=1,factorZ=40/3,factor=1.75);

!rotate([0,0,90])
BaseSlave(ancho=40,factorX=1,factorZ=40,factor=1.75);
*/