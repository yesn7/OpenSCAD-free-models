
include<./../CajaParametrizable/cajaA3.scad>;


module retrato(lonZ=190,lonX=120,espesor=1.75,tol=1,marco=5,texto1="IDEA 1.61",texto2="IDEA 1.61",texto3="IDEA 1.61",texto4="IDEA 1.61",MDF=0,divisiones=6){
    hipot=sqrt(pow(lonZ/2,2)+pow(lonX/4,2));
angulo=asin((lonX/4)/(hipot));
    ajuste=2*espesor;
    
    if(MDF==0){
translate([0,espesor*(3/2),(lonZ/2)+espesor+tol])
rotate([angulo,0,0])
difference(){
    //marco
    cube([lonX+(2*espesor)+(2*tol),espesor*3+(2*tol),lonZ+(2*espesor)+(2*tol)],center=true);
    //interior
   cube([lonX+(2*tol),espesor+(2*tol),lonZ+(2*tol)],center=true);
    //vista
    translate([0,(3*espesor)/2,0])
    cube([lonX+(2*tol)-(2*marco),espesor*3,lonZ+(2*tol)-(2*marco)],center=true);
    
     //interior Introducir
    translate([0,0,lonZ/2])
   cube([lonX+(2*tol),espesor+(2*tol),lonZ+(2*tol)],center=true);
  
    
}



rotate([0,-90,0])
translate([0,-lonX/4,-espesor/2])
linear_extrude(height=espesor)
polygon([[ajuste,lonX/2],[lonZ,0],[ajuste,0]]);



//rotate([0,180,90])
translate([0,(lonX/4)+(espesor*3)+(2*tol),espesor*4])
rotate([90-angulo,0,180])
translate([0,-espesor-tol/2,-espesor*1.5])
linear_extrude(height=espesor)
 text(size=6,font = "Simplex",texto1,halign ="center");
 

translate([0,(lonX/4)+(espesor*3)+(2*tol),espesor*4])
rotate([90-angulo,0,180])
translate([0,-(marco/2)+tol-(tol/4)+lonZ,-espesor*1.5])
linear_extrude(height=espesor)
 text(size=6,font = "Simplex",texto2,halign ="center");
 
 translate([-lonX/4,(lonX/4)-espesor,espesor*4])
 rotate([90+angulo,0,0])
 linear_extrude(height=espesor)
 text(size=6,font = "Simplex",texto3,halign ="center");
 
 translate([lonX/4,(lonX/4)-espesor,espesor*4])
 rotate([90+angulo,0,0])
 linear_extrude(height=espesor)
 text(size=6,font = "Simplex",texto4,halign ="center");
 }//fin 3D
 else if(MDF==1){
    
     
     
     ladoLargoX=lonX+(2*espesor)+(2*tol);
     ladoAnchoY=espesor*3+(2*tol);
     ladoAltoZ=lonZ+(2*espesor)+(2*tol);
     
      diente=ladoAltoZ/divisiones;

     difference(){
     cajaSCTapa(largo=ladoLargoX,ancho=ladoAnchoY,alto=ladoAltoZ,espesor=espesor,dientes=divisiones,modo=0);
         translate([0,(ladoAltoZ/2)+(ladoAnchoY/2)+(espesor*2.5)])
         square([lonX-(2*marco),lonZ-(2*marco)],center=true);
         
     //agujeros para base
              translate([ladoLargoX+(espesor*2.5),(ladoAnchoY/2)+(espesor*2.5)+(diente/2)])
     for(i=[0:(divisiones-1)/2])
     translate([0,diente*2*i])
     square([espesor,diente],center=true);
         
         }
     
         


   translate([(ladoLargoX*2)+(6.27*espesor)-(lonX/2),(ajuste+(diente/2))])
         rotate(-angulo-0.55)
         translate([0,espesor])
     for(i=[0:(divisiones-1)/2])
     translate([0,diente*2*i])
     square([espesor,diente],center=true);
         
         
   translate([(ladoLargoX*2)+(5*espesor),0])
         rotate(90)
         polygon([[ajuste,lonX/2],[lonZ,0],[ajuste,0]]);

    
     /*
      translate([ladoLargoX+(espesor*2.5),(ladoAltoZ/2)+(ladoAnchoY/2)+(espesor*2.5)])
     for(i=[0:(divisiones-1)/2])
     translate([0,diente*2*i])
     square([espesor,diente],center=true);
     */
     
 }
 
 
 }//fin modulo
 
 
 //####RENDERIZADOS####
 
 
 retrato(lonZ=190,lonX=120,espesor=3,tol=1,marco=5,hipot=sqrt(pow(lonZ/2,2)+pow(lonX/4,2)),texto1="E & K & Y & C  05 / 10 / 17",texto2="",texto3="",texto4="",MDF=0,divisiones=6);