
diamEje=7.89;
//tol=0.5;
tol=-0.1;
alturaEje=100;
fondoCaja=150;
espacioFondo=10;
esp=3;
tam=1.5;
anchoBalero=7;
diamBaleroVirtual=19;
$fn=20;

//eje
difference(){
   cylinder(d=diamEje-(2*tol),h=alturaEje+anchoBalero-7.9);
    translate([0,0,alturaEje+anchoBalero-(5.36+tol+7.9)])
    recoPot(tol=0.5);
}

//tope eje
translate([0,0,anchoBalero+(anchoBalero/2)])
cylinder(d=diamBaleroVirtual,h=tam);

//estructura 90 grados
translate([0,0,anchoBalero+(alturaEje/2)])
rotate([0,90,0])
cylinder(d=diamEje-(2*tol),h=fondoCaja-espacioFondo-(espacioFondo*2));

//flotador
translate([fondoCaja-espacioFondo-(espacioFondo*2)-30,0,anchoBalero+(alturaEje/2)])
minkowski(){
    
    cube([20,0.1,0.1]);
    sphere(r=10);
    
}

//potenciometro

module recoPot(tol=0.5){
    difference(){
       cylinder(d=3.48+(2*tol),h=5.36+tol);

    translate([0,3+1+tol,3])
    cube([6,6,6],center=true);
        
        translate([0,-(3+1)-tol,3])
    cube([6,6,6],center=true);
    }
}//fin module
$fn=100;

/*
//prueba eje
!rotate([180,0,0])
difference(){
cylinder(d=diamEje-(2*tol),h=1.5*anchoBalero);
    recoPot(tol=0.65);
}
*/