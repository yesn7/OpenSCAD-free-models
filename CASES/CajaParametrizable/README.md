# Parameterizable box

![](./PNG/CajaParametrizable.png)

## Summary/Descripción 


The following code is used to make DXF drawings that can be used to machine MDF or acrylic boxes (materials that can be used in CNC design machines or laser cutting) and can be entered values of thickness, number of joints, length in dimension in "x" "y" & "z", in addition to this it can be included folding lid or not.
La sugerencia de uso que damos, es que se puede usar para proyecto de construcción con los planos, objetos armables, cajas variantes que puede incluir o no, una tapa.

El siguiente código sirve para hacer planos en DXF que sirvan para maquinar cajas de MDF o acrílico (materiales que se puedan ocupar en máquinas de diseño CNC o corte láser) y se les pueda ingresar valores de espesor, número de empalmes, longitud en dimensión en "x" "y" & "z", además de ésto se le puede incluir tapa abatible o no.
La sugerencia de uso que damos, es que se puede usar para proyecto de construcción con los planos, objetos armables, cajas variantes que puede incluir o no, una tapa.

## Guide/Guia
 
Some projects need others to work. CASEgamma and Utilities/Utilidades are an example of projects that are used by others to run, it is recommended to download the complete repository to ensure the correct rendering of projects. 
you can clone it or download it in.zip from https://gitlab.com/idea161/OpenSCAD-free-models/
Projects are licensed under the GPLV3 license, which is consistent with the 4 fundamental freedoms of free software, so feel free to study the code, replicate it and modify it at your convenience as this repository contributes to free software!

Algunos proyectos necesitan de otros para poder funcionar. CASEgamma y Utilities/Utilidades son un ejemplo de proyectos que son utilizados por otros para poder funcionar, se recomienda descargar el repositorio completo para garantizar el correcto renderizado de los proyectos 
para ello puedes clonarlo o descargarlo en .zip desde https://gitlab.com/idea161/OpenSCAD-free-models/
Los proyectos tienen la licencia GPLV3, la cual concuerda con las 4 libertades fundamentales del software libre, así que siéntete libre de estudiar el código, replicarlo y modificarlo a tu conveniencia ya que éste repositorio ¡contribuye al software libre!

## IDEA 1.61

![](./PNG/idea.png)

https://idea161.org/projects/caja-parametrizable-2/

https://www.thingiverse.com/thing:2829146/

https://www.youtube.com/channel/UCXwIW7z5ys3FRVoOducFSeQ

contacto@idea161.org

##Social

https://www.instagram.com/idea161/

https://www.facebook.com/idea161/

TelegramChannel t.me/idea161

## LIDSOL

![](./PNG/LidsolGris.png)

Web site: https://lidsol.org


For more objects and models like this, follow us on:
Para más materiales y modelos como éste, síguenos en:

(Available until May 2018/Disponible hasta Mayo 2018).
https://github.com/LIDSOL/OpenSCAD-free-models/

(Available from May 2018 onwards/Disponible a partir de Mayo 2018).
https://gitlab.com/idea161/OpenSCAD-free-models/

# Hints/Recomendaciones

1. As the model is modifiable, the dimensions that will be required in your case must be checked very well.
2. The type of material with which the equine is to be manufactured (ABS or PLA) 

La eficacia y calidad de la pieza dependerá de 2 factores a considerar:
1. Como el modelo es modificable, hay que considerar muy bien las dimensiones que se vayan a requerir en su caso.
2. El tipo de material con el que se vaya a fabricar el esquinero (ABS o PLA).

## Applications/Aplicaciones

#### Aesthetic/Estetico

If the use of this model is focused on any artistic project: The design, the aesthetics and its characteristics are free, go to the taste of the user.

Si el uso de este modelo va enfocado a algún proyecto artístico: El diseño, la estética y sus características son libres, van al gusto del usuario.


### Engineering/Ingeniería

If the use of this model is focused on any engineering project:
Mainly consider the load factor, dimensions required and necessary for a good support and operation, as well as the design and aesthetics. 

Si el uso de este modelo va enfocado a algún proyecto ingenieril: Principalmente considerar el factor de carga, dimensiones requeridas y necesarias para un buen soporte y funcionamiento, así como el diseño y su estética.     

### Home/Hogar

If the use of this model is focused on domestic or daily use material: Mainly consider the load factor and resistance of the material, dimensions required and the type of material to be manufactured, as well as the design to the user's taste.

Si el uso de este modelo va enfocado como material domestico o de uso diario: Principalmente considerar el factor de carga y resistencia del material, dimensiones requeridas y el tipo de material del que se va a fabricar, así como el diseño al gusto del usuario.   
   
## Disclaimer/Responsiva
          
This contribution is for the purpose of making a contribution to the community and helping with construction.
After reading the Factors to consider, we are not 
responsible for any inconvenience, hoping that these models will be to your liking.
    
Esta contribución es con el fin de hacer un aporte a la comunidad y de ayuda a la construcción.
Después de leer los Factores a considerar, nosotros no nos hacemos responsables por algún inconveniente, esperando que estos modelos sean de su agrado.

## Legal Notice/Anuncio Legal

IDEA1.61 LEGAL NOTICE © Copyright IDEA1.61 2018. All rights reserved. IDEA1.61 logo is a copyrighted work.
DITAC LEGAL NOTICE © Copyright Ditac 2018. All rights reserved. Ditac logos is a copyrighted work.

IDEA1.61 AVISO LEGAL © Copyright IDEA1.61 2018. Todos los derechos reservados. El logotipo de IDEA1.61 es una obra protegida por derechos de autor.
AVISO LEGAL DE DITAC © Copyright Ditac 2018. Todos los derechos reservados. El logotipo de Ditac es una obra protegida por derechos de autor.
