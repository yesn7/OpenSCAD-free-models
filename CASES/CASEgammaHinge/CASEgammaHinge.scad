include<./../../Utilidades/Utilities.scad>;
include<./../../Mecanica/OpenSCAD/Hinge.scad>;
include<./../CASEgamma/CASEgamma.scad>;
include<./../../Endurance/OpenSCAD/Endurance.scad>;
include<./../../Logos/LogoIdea161.scad>;
include<./../../Logos/StarWars.scad>;

/*
####Explicación de variables#####
Largo=210 -> Largo del case
Ancho=60 -> Ancho del case
Tapa=11   -> Alto de la tapa
Fondo=(22*(3/2))/2 -> Alto del fondo del case
espesor=1.5  -> espesor del material
minkow=3   -> redondeador de las esquinas "minkowski"
sepCaHi=1   ->   separacion del case y hinge (bisagra)
texto1="Idea 1.61"   -> ingresa un texto del lado derecho
texto2="Nombre"    -> ingresa un texto del lado izquierdo
fuente=6         -> tamaño del texto
ajuste=15       -> ajuste en posicion de texto
*/

module CASEgammaHingeAlfa(diamEje=3.5,tol=2.1,Largo=210,Ancho=60,Tapa=9,Fondo=9,espesor=1.5,minkow=3,sepCaHi=1,texto1="Idea 1.61",texto2="Nombre",fuente=6,logo=1,angulo=90,estrella=1){
    
lonX_A=Largo;
lonY_A=Ancho;
lonZ_A=Tapa*2;

lonX_B=Largo;
lonY_B=Ancho;
lonZ_B=Fondo*2;

//Distancia necesaria para centrar correctamente la bisagra con objetos
DT=diamEje+(tol*2)+(espesor*2);


rotate([0,90,0])
Hinge(diamEje=diamEje,anchoBisagra=Largo,utol=0.1,tol=0.7,espesor=espesor+0.5,facSepara=2.5,espacioPlano=2,angulo=-angulo,separador=sepCaHi+espesor,clip=2,viaje=Ancho+(espesor*4));

//ESTAPIEZA NO SE MUEVE
translate([lonY_A/2+(DT/2)+sepCaHi,0,0])
rotate([0,90,90])

if(logo==0){
 //logo idea
    translate([0,-20,lonZ/2-espesor/2])
    linear_extrude(height=espesor)
    Idea(0.18);
}
difference(){
    CASEgamma(lonX=lonX_A,lonY=lonY_A,lonZ=lonZ_A,espesor=espesor,diametroPoste=0,tornillo=0,parte=1,minkow=minkow,soporteA=0,soporteB=0,soporteC=0,soporteD=0);

if(estrella==1){
 orillas=4;
    
    translate([0,0,-lonZ_A*2])
    linear_extrude(height=lonZ_A*4)
    difference(){
    square([lonX_A-(orillas/2),lonY_A-(orillas/2-(2*minkow))],center=true);
RecortadorEstrella(lonX=lonX_A, lonY=lonY_A,orillas=orillas,redondeadorPoly=2);
    }
        }
  

}

rotate(angulo-90)
translate([0,lonY_B/2+(DT/2)+sepCaHi,0])
rotate([0,90,0])

difference(){
CASEgamma(lonX=lonX_B,lonY=lonY_B,lonZ=lonZ_B,espesor=espesor,diametroPoste=0,tornillo=0,parte=1,minkow=minkow,soporteA=0,soporteB=0,soporteC=0,soporteD=0);

if(estrella==1){
 orillas=4;
    
    translate([0,0,-lonZ_B*2])
    linear_extrude(height=lonZ_B*4)
    difference(){
    square([lonX_B-(orillas/2),lonY_B-(orillas/2-(2*minkow))],center=true);
RecortadorEstrella(lonX=lonX_B, lonY=lonY_B,orillas=orillas,redondeadorPoly=2);
    }

}

}

//Endurance en Relieve
if(logo==1){
    translate([lonY_A/2+(DT/2)+sepCaHi,-lonZ_A/2-espesor,0])
    rotate([0,90,90])
    linear_extrude(height=espesor*2)
    resize([(lonY_A*0.75)-(2*minkow),(lonY_A*0.75)-(2*minkow)])
    projection()
    rotate([90,0,0])
    GanzEndurance();
    
    //cilindro base
    if(estrella==1){
    translate([lonY_A/2+(DT/2)+sepCaHi,-(lonZ_A/2),0])
    rotate([0,90,90])
    cylinder(d=lonY_A*0.75,h=espesor);
    }
}

//#####Textos#####

//medidas obtenidas desde "echo" de funcion hinge y 3mm adicionales por el tamaño de texto
translate([lonY_A/2+(DT/2)+sepCaHi+(fuente/2),-lonZ_A/2+espesor,(lonX_A)/4])
rotate([0,270,90])
 linear_extrude(height=espesor*1.5)
 //tamaño del texto en [mm]
text(size=fuente,font = "Simplex",texto2,halign ="center");

//medidas obtenidas desde "echo" de funcion hinge y 3mm adicionales por el tamaño de texto
translate([lonY_A/2+(DT/2)+sepCaHi+(fuente/2),-lonZ_A/2+espesor,-((lonX_A)/4)])
rotate([0,270,90])
 linear_extrude(height=espesor*1.5)
 //tamaño del texto en [mm]
text(size=fuente,font = "Simplex",texto1,halign ="center");


}//fin CASEgammaHingeAlfa


module CASEgammaHingeBeta(diamEje=3.5,tol=2.1,Largo=90,Ancho=60,Fondo=20,espesor=1.5,minkow=3,sepCaHi=1,texto1="Idea 1.61",texto2="Nombre",fuente=6,logo=1,angulo=90,divLargo=4,divAncho=3,logo=1,soportes=0){

//Distancia necesaria para centrar correctamente la bisagra con objetos
DT=diamEje+(tol*2)+(espesor*2);
//diamEje=diamEje,anchoBisagra=Largo,utol=0.1,tol=0.7,espesor=espesor+0.5,facSepara=2.5,espacioPlano=2,angulo=-angulo,separador=sepCaHi+espesor,clip=2,viaje=Ancho+(espesor*4))
Hinge(diamEje=diamEje,anchoBisagra=Largo,utol=0.1,tol=0.7,espesor=espesor+0.5,facSepara=2.5,espacioPlano=2,angulo=angulo,separador=sepCaHi+espesor,clip=3,viaje=Ancho+(DT/2));
 
//se le agrega 0.85mm a translte en "Y" por ser constante la variable facSepara en Hinge
translate([0,-DT/2+0.85,(Ancho/2)+(DT/2)+(tol/4)])
rotate([-90,0,0])
union(){
    
    if(soportes==1){
CASEgamma(lonX=Largo,lonY=Ancho,lonZ=Fondo*2,espesor=espesor,diametroPoste=0,tornillo=0,parte=1,minkow=minkow,soporteA=1,soporteB=0,soporteC=1,soporteD=0);rotate([angulo,0,0]);
        
    }
    else{
       CASEgamma(lonX=Largo,lonY=Ancho,lonZ=Fondo*2,espesor=espesor,diametroPoste=0,tornillo=0,parte=1,minkow=minkow,soporteA=0,soporteB=0,soporteC=0,soporteD=0);rotate([angulo,0,0]); 
        
        }
    
    //factor y 1.25 para evitar inconsistencias en uniones (se recomienda revisar y corregir)
    medZ=(Fondo)+(espesor*1.25)-(minkow);
     //minkow/2 para evitar inconsistencias en uniones (se recomienda revisar y corregir)
    medY=Ancho-(2*espesor)+(minkow/2);
    medX=Largo-(2*espesor)+(minkow/2);
    unidadX=(Largo-(2*espesor))/divLargo;
    unidadY=(Ancho-(2*espesor))/divAncho;
    translate([-((Largo-(2*espesor))/2)+unidadX,0,0])
    for(i=[0:(divLargo-2)])
    //translate([-espesor/2,-medYAncho/2,-medZ])
    translate([-espesor/2+(unidadX*i),-medY/2,-medZ]) 
    cube([espesor,medY,medZ]);
    
    translate([0,-((Ancho-(2*espesor))/2)+unidadY,0])
    for(i=[0:(divAncho-2)])
    //translate([-espesor/2,-medYAncho/2,-medZ])
    translate([-medX/2,-espesor/2+(unidadY*i),-medZ]) 
    cube([medX,espesor,medZ]);
   
}
    


if(logo==1){
 //logo idea
    
    rotate([angulo,0,0])
    translate([0,Ancho/2,DT/4+espesor])
     rotate([180,0,0])
    linear_extrude(height=espesor*2)
    Idea(0.25);
}

else if(logo==2){
 rotate([angulo,0,0])
    translate([0,Ancho/2,DT/4+espesor])
     rotate([180,0,0])
    translate([0,-Ancho*((1.5)/16),0])
    linear_extrude(height=espesor*2)
PrimeraOrden(radio=Ancho/4);

}

else if(logo==3){
     rotate([angulo,0,0])
    translate([0,Ancho/2,DT/4+espesor])
     rotate([180,0,0])
     translate([0,-Ancho*((1.5)/16),0])
    //MEDIA EXTRA
    linear_extrude(height=espesor*1.5)
Republica(radio=Ancho/4);

}
else if(logo==4){
rotate([angulo,0,0])
    translate([0,Ancho/2,DT/4+espesor])
    rotate([180,0,0])
     translate([0,-Ancho*((1.5)/16),0])
 linear_extrude(height=espesor*2)
    resize([Ancho/2,Ancho/2])
    projection()
    rotate([90,0,0])
    GanzEndurance();
}

else if(logo==5){
  rotate([angulo,0,0])
    translate([0,Ancho/2,DT/4+espesor])
    rotate([180,0,0])
     translate([0,-Ancho*((1.5)/16),0])
    resize([Ancho/2,Ancho/2,espesor*2])
    import("./../Logos/STL/aperture.stl");

}

else if(logo==6){
   rotate([angulo,0,0])
    translate([0,Ancho/2,DT/4+espesor])
    rotate([180,0,0])
     translate([0,-Ancho*((1.5)/16),0])
    resize([Ancho/2,Ancho/2,espesor*2])
    import("./../Logos/STL/artic_monkeys.stl");


}

else if(logo==7){
   rotate([angulo,0,0])
    translate([0,Ancho/2,DT/4+espesor])
    rotate([180,0,0])
     translate([0,-Ancho*((1.5)/16),0])
    resize([Ancho/2,Ancho/2,espesor*2])
    import("./../Logos/STL/death_star_logo.stl");


}




else if(logo==8){
    rotate([angulo,0,0])
    translate([0,Ancho/2,DT/4+espesor])
    rotate([180,0,0])
     translate([0,-Ancho*((1.5)/16),0])
    resize([Ancho/2,Ancho/2,espesor*2])
    import("./../Logos/STL/eva_01_logo.stl");

}

else if(logo==9){
    rotate([angulo,0,0])
    translate([0,Ancho/2,DT/4+espesor])
    rotate([180,0,0])
     translate([0,-Ancho*((1.5)/16),0])
    resize([Ancho/2,Ancho/2,espesor*2])
     import("./../Logos/STL/falcon_logo.stl");

}

else if(logo==10){
    rotate([angulo,0,0])
    translate([0,Ancho/2,DT/4+espesor])
    rotate([180,0,0])
     translate([0,-Ancho*((1.5)/16),0])
    resize([Ancho/2,Ancho/2,espesor*2])
    import("./../Logos/STL/fd_logo.stl");

}

else if(logo==11){
    rotate([angulo,0,0])
    translate([0,Ancho/2,DT/4+espesor])
    rotate([180,0,0])
     translate([0,-Ancho*((1.5)/16),0])
    resize([Ancho/2,Ancho/2,espesor*2])
    import("./../Logos/STL/fq_logo.stl");

}

else if(logo==12){
    rotate([angulo,0,0])
    translate([0,Ancho/2,DT/4+espesor])
    rotate([180,0,0])
     translate([0,-Ancho*((1.5)/16),0])
    resize([Ancho/2,Ancho/2,espesor*2])
    import("./../Logos/STL/inge_logo.stl");

}

else if(logo==13){
    rotate([angulo,0,0])
    translate([0,Ancho/2,DT/4+espesor])
    rotate([180,0,0])
     translate([0,-Ancho*((1.5)/16),0])
    resize([Ancho/2,Ancho/2,espesor*2])
    import("./../Logos/STL/iron_man_logo.stl");

}

else if(logo==14){
    rotate([angulo,0,0])
    translate([0,Ancho/2,DT/4+espesor])
    rotate([180,0,0])
     translate([0,-Ancho*((1.5)/16),0])
    resize([Ancho/2,Ancho/2,espesor*2])
   import("./../Logos/STL/jedi_logo.stl");

}

else if(logo==15){
    rotate([angulo,0,0])
    translate([0,Ancho/2,DT/4+espesor])
    rotate([180,0,0])
     translate([0,-Ancho*((1.5)/16),0])
    resize([Ancho/2,Ancho/2,espesor*2])
  import("./../Logos/STL/mand_logo.stl");

}

else if(logo==16){
    rotate([angulo,0,0])
    translate([0,Ancho/2,DT/4+espesor])
    rotate([180,0,0])
     translate([0,-Ancho*((1.5)/16),0])
    resize([Ancho/2,Ancho/2,espesor*2])
   import("./../Logos/STL/nerv_logo.stl");

}

else if(logo==17){
   rotate([angulo,0,0])
    translate([0,Ancho/2,DT/4+espesor])
    rotate([180,0,0])
     translate([0,-Ancho*((1.5)/16),0])
    resize([Ancho/2-12,Ancho/2,espesor*2])
import("./../Logos/STL/noFace.stl");

}

else if(logo==18){
    rotate([angulo,0,0])
    translate([0,Ancho/2,DT/4+espesor])
    rotate([180,0,0])
     translate([0,-Ancho*((1.5)/16),0])
    resize([Ancho/2,Ancho/2,espesor*2])
  import("./../Logos/STL/pumas_logo.stl");

}

else if(logo==19){
    rotate([angulo,0,0])
    translate([0,Ancho/2,DT/4+espesor])
    rotate([180,0,0])
     translate([0,-Ancho*((1.5)/16),0])
       resize([Ancho/2,Ancho/2,espesor*2])
   import("./../Logos/STL/rasp_logo.stl");

}

else if(logo==20){
    rotate([angulo,0,0])
    translate([0,Ancho/2,DT/4+espesor])
    rotate([180,0,0])
      translate([0,-Ancho*((1.5)/16),0])
    resize([Ancho/2,Ancho/2,espesor*2])
  import("./../Logos/STL/Rebel_Alliance_logosvg.stl");

}

else if(logo==21){
    rotate([angulo,0,0])
    translate([0,Ancho/2,DT/4+espesor])
    rotate([180,0,0])
      translate([0,-Ancho*((1.5)/16),0])
    resize([Ancho/2,Ancho/2,espesor*2])
  import("./../Logos/STL/tie_vader.stl");

}

else if(logo==22){
    rotate([angulo,0,0])
    translate([0,Ancho/2,DT/4+espesor])
    rotate([180,0,0])
      translate([0,-Ancho*((1.5)/16),0])
    resize([Ancho/2,Ancho/2,espesor*2])
    import("./../Logos/STL/tux_logo.stl"); 

}


else if(logo==23){
    rotate([angulo,0,0])
    translate([0,Ancho/2,DT/4+espesor])
    rotate([180,0,0])
      translate([0,-Ancho*((1.5)/16),0])
    resize([Ancho/2,Ancho/2,espesor*2])
import("./../Logos/STL/UNAM.stl");

}

 rotate([angulo,0,0])
    translate([0,Ancho/4,DT/4+espesor])
rotate([180,0,0])
linear_extrude(height=espesor*1.5)
 text(size=fuente,font = "Simplex",texto1,halign ="center");

rotate([angulo,0,0])
    translate([0,Ancho,DT/4+espesor])
rotate([180,0,0])
linear_extrude(height=espesor*1.5)
 text(size=fuente,font = "Simplex",texto2,halign ="center");
    
}

//####CARTERA####

module CarteraAlfa(estrella=0){
espesor=1.5;
Largo=95;
Ancho=60;
//Largo=40;
//Ancho=40;
//FactorSolapa=0.25;
estrella=0;
    orillas=4;

CASEgammaHinge(Largo=Largo,Ancho=Ancho,Tapa=(17.5/2)+2,Fondo=(17.5/2)+2,espesor=espesor,minkow=3,sepCaHi=1,texto1="Idea",texto2="1.61",fuente=6,ajuste=8.5,logo=1,estrella=estrella);

medX=(Ancho-(2*espesor))*0.5;
medY=(Largo-(2*espesor))*1;
medZ=espesor;

translate([(medX*1.5)+(17.5/2)-(espesor*1.2),-espesor/2,0])
rotate([90,0,0])
difference(){

cube([medX,medY,medZ],center=true);
    
    
    translate([0,0,-medZ*2])
    linear_extrude(height=medZ*4)
    difference(){
    square([medX-(orillas/2),medY-(orillas/2)],center=true);
        translate([medX/2,0,0])
RecortadorEstrella(lonX=medX*2, lonY=medY,orillas=orillas,redondeadorPoly=2);
    }
    
}


rotate(90)
translate([medX/2+(17.5/2)-(espesor*1.2),espesor/2,0])
rotate([90,0,0])
difference(){

cube([medX,medY,medZ],center=true);
    
    
    
    translate([0,0,-medZ*2])
    linear_extrude(height=medZ*4)
    difference(){
    square([(medX-(orillas/2)),medY-(orillas/2)],center=true);
        translate([-medX/2,0,0])
RecortadorEstrella(lonX=medX*2, lonY=medY,orillas=orillas,redondeadorPoly=2);
    }
    
} 


}//fin cartera

//#####TABLET HOLDER####

module TabletHolder(){
Largo=110;
//sepCaHi=0;
espesor=1.5;

Base=140;
diamEje=3.5;

difference(){
rotate([0,90,15])
Hinge(diamEje=diamEje,anchoBisagra=Largo,tol=0.6+0.1,espesor=2,facSepara=2.5,espacioPlano=Base,angulo=210,separador=0,clip=0,viaje=0);
   
   
   translate([0,-Base/2-(diamEje*1.5),Largo/4])
     
    rotate([-45,0,0])
    translate([0,-40,0])
recortadorMink(lonX=Base,lonY=Base,lonZ=Largo+40,esf=10);

}
}//fin tablet holder


//BookHolder
module BookHolder(){
Largo=110;
//sepCaHi=0;
espesor=1.5;

Base=180;
diamEje=3.5;

difference(){
rotate([0,90,15])
Hinge(diamEje=diamEje,anchoBisagra=Largo,tol=0.6+0.1,espesor=2,facSepara=2.5,espacioPlano=Base,angulo=210,separador=0,clip=0,viaje=0);
   
   
   translate([0,-Base/2-(diamEje*1.5)+30,Largo/3])
     
    rotate([-25,0,0])
    translate([0,-30,0])
recortadorMink(lonX=Base,lonY=Base/2,lonZ=Largo+40,esf=10);

}

}
//fin book holder


//####RENDERIZADOS####


//No deben existir renders para la compatibilidad con otros proyectos (debugging)

$fn=50;

//CarteraAlfa(estrella=0);
//TabletHolder();
//BookHolder();

//Configuraciones para lapicero
//CASEgammaHingeAlfa(Largo=210,Ancho=60,Tapa=11,Fondo=(22*(3/2))/2,espesor=1.5,minkow=3,sepCaHi=1,texto1="Idea 1.61",texto2="Nombre",fuente=6,logo=1,angulo=90,estrella=0);


//organizadorIdea
//CASEgammaHingeBeta(diamEje=3.5,tol=2.1,Largo=160,Ancho=80,Fondo=30,espesor=1.5,minkow=3,sepCaHi=1,texto1="Idea 1.61",texto2="PabloVC",fuente=6,logo=1,angulo=45,divLargo=4,divAncho=3,soportes=0);
 
 //caja audifonos
 //CASEgammaHingeBeta(diamEje=3.5,tol=2.1,Largo=65,Ancho=65,Fondo=30,espesor=1.75,minkow=3,sepCaHi=1,texto1="",texto2="Penserbjorne",fuente=6,logo=8,angulo=45,divLargo=0,divAncho=0,soportes=0);
 
 //CASEgammaHingeBeta(diamEje=3.5,tol=2.1,Largo=65,Ancho=65,Fondo=30,espesor=1.5,minkow=3,sepCaHi=1,texto1="",texto2="Mariana",fuente=6,logo=17,angulo=45,divLargo=0,divAncho=0,soportes=1);
 
 
  //caja LEGO
 CASEgammaHingeBeta(diamEje=3.5,tol=2.1,Largo=90,Ancho=90,Fondo=35,espesor=1.25,minkow=3,sepCaHi=1,texto1="",texto2="Pablo VC",fuente=6,logo=3,angulo=45,divLargo=0,divAncho=3,soportes=0);
 
 



