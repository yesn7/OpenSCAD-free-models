include<./../../Componentes/boards.scad>;
include<./../CASEgamma/CASEgamma.scad>;
//LOGOS
include<./../../Logos/LogoIdea161.scad>;
include<./../../Logos/LogoMonequi.scad>;
//include<./../Logos/LogoDitacRegSCorch.scad>;
//Recortadores
include<./../../Utilidades/Utilities.scad>;



module caseArduino(parte=0,logo=1,texto1="IDEA",texto2="1.61",puntos=10){
lonX=60;
lonY=75;
lonZ_A=26.5;
lonZ_B=12;
espesor=1.75;
diametroPoste=5;
tornillo=2.5;

//logos
tamaNioLogos=0.3;
//postes arduino
alturaPostesArduino=12;
diametroPosteArduino=6;
tornilloArduino=3.2;
tol=0.5;
//recorrimiento=2;
minkow=3;



if(parte==0){
   difference(){
       //translate([0,0,-recorrimiento])
       union(){
       CASEgamma(lonX=lonX,lonY=lonY,lonZ=lonZ_A,espesor=espesor,diametroPoste=0,tornillo=0,parte=0,minkow=minkow,soporteA=0,soporteB=0,soporteC=0,soporteD=0);
       
   
           
           //####Soportes para tornillos Arduino
                    
            //Soporte(1,1,B)
    translate([lonX/2-espesor-1,lonY/2-diametroPoste-12.25-0.6,(lonZ_A-2*espesor)/4])
    cube([diametroPoste/2+1,diametroPoste/2,lonZ_A/2-espesor],center=true);
                 
            //Soporte(-1,1,B)
    translate([-lonX/2+espesor+1,lonY/2-diametroPoste-12.25,(lonZ_A-2*espesor)/4])
    cube([diametroPoste/2,diametroPoste/2,lonZ_A/2-espesor],center=true);
    
           //seccion de soportes con menos distancia entre sI
           
     //Soporte(1,-1,A)
    translate([lonX/2-diametroPoste-16,-lonY/2+espesor+1,(lonZ_A-2*espesor)/4])
    cube([diametroPoste/2,diametroPoste/2,lonZ_A/2-espesor],center=true);
           
              //Soporte(-1,-1,A)
    translate([-lonX/2+diametroPoste+6,-lonY/2+espesor+1,(lonZ_A-2*espesor)/4])
    cube([diametroPoste/2,diametroPoste/2,lonZ_A/2-espesor],center=true);
      
      
               //###Tornillos arduino#####
       /*
       alturaPostesArduino=12;
         diametroPosteArduino=6;
           tornilloArduino=3.2;
*/     
      translate([-53.3/2,68.6/2,0])
       rotate(-90)
union(){    
   
translate([13.6,3,0])
   
poste(diametroPosteArduino,tornilloArduino,lonZ_A-espesor/2);
    
     

translate([13.6+51.9,3+4.7,0])
poste(diametroPosteArduino,tornilloArduino,lonZ_A-espesor/2);


translate([13.6+51.9,3+4.7+27.9,0])
poste(diametroPosteArduino,tornilloArduino,lonZ_A-espesor/2);



//poste que se ajustO
translate([13.6+1.1,3+4.7+27.9+15.2,0])
  poste(diametroPosteArduino-1.5,tornilloArduino-0.5,lonZ_A-espesor/2);  
    /*difference(){
poste(diametroPosteArduino,tornilloArduino-0.5,lonZ_A-espesor/2);
        //con la coordenada x de este translate se ajusta el corte de este poste
       translate([1.6,-15,0])
        cube([30,30,60]);
}*/
    
    
}//fin union postes
           
           }//fin union postes y CASEgamma
           
      //recorte Arduino UNO   
      translate([0,0,4.5]) 
       rotate(-90)
      ArduinoUNO(taladroEntradas=10,tol=tol,ajustePostR=1,modo=1);
       
       /*
       //transformacion inicial
       translate([-53.3/2,68.6/2,-12.5])
       rotate(-90)
       //se aumento 8 en eje y y se redujo 1mm en eje x del la coordenada del último tornillo
      translate([13.6+1.1-1,3+4.7+27.9+15.2+8,0])
       cylinder(r=1,$fn=100,h=lonZ*2);
       */
           
        
        
             //####LOGOS####

//tamaNioLogos=0.5;

/*translate([-20*tamaNioLogos+2,-lonY/4,lonZ_A/2-espesor/2])
union(){
   linear_extrude(height=espesor/2)
   Ditac(tamaNioLogos);

//###RENDERIZADOS###
    translate([30*tamaNioLogos,5*tamaNioLogos])
    linear_extrude(height=espesor/2)
    Idea(tamaNioLogos/4);
}*/


if(logo==1){
translate([0,-8,lonZ_A/2-espesor/2])
    linear_extrude(height=espesor)
    Idea(tamaNioLogos);
}
    else if(logo==2){
        translate([0,-8,lonZ_A/2-espesor/2])
      //linear_extrude(height=espesor)
    //MonequiLogo(radio=12);
        scale([1.5,1.5,1])
        import("./../../Logos/STL/LogoMonequi.stl");
    }

//####TEXTO###

translate([0,lonY/4+4,lonZ_A/2-espesor/2])
linear_extrude(height=espesor)
 text(size=puntos,font = "Simplex",texto1,halign ="center");
 translate([0,lonY/4-12+4,lonZ_A/2-espesor/2])
linear_extrude(height=espesor)
 text(size=puntos,font = "Simplex",texto2,halign ="center");
        
        
           
           
           
           
  }//fin difference tarjetas y detalles
  
 //###Texto Entradas###
  /*
  translate([-lonX/2+espesor/2,10,lonZ_A/4-2])
rotate([90,0,-90])
linear_extrude(height=espesor)
 text(size=3,font = "Simplex","POWER");
 
  translate([-lonX/2+espesor/2,-10,lonZ_A/4-2])
rotate([90,0,-90])
linear_extrude(height=espesor)
 text(size=3,font = "Simplex","ANALOG IN");
 
  rotate(180)
translate([-lonX/2+espesor/2,12,lonZ_A/4-2])
rotate([90,0,-90])
linear_extrude(height=espesor)
 text(size=3,font = "Simplex","DIGITAL");
*/

//soporte parte solitaria entradas
  translate([-5,(lonY/2)-(espesor*0.75),(lonZ_A/2)-(espesor*0.75)])
  rotate([0,90,180])
  translate([0,0,-espesor/2])
  linear_extrude(height=espesor)
  polygon([[0,0],[(lonZ_A/2)-espesor,0],[0,(lonZ_A/4)-espesor]]);

}//fin parte 0

if(parte==1){
   difference(){
       union(){
           ajustePoste=1;
          // translate([0,0,-recorrimiento])
         CASEgamma(lonX=lonX,lonY=lonY,lonZ=lonZ_B,espesor=espesor,diametroPoste=0,tornillo=0,parte=1,minkow=minkow,soporteA=0,soporteB=0,soporteC=0,soporteD=0);
           
             
       //###Tornillos arduino#####
       /*
       alturaPostesArduino=12;
         diametroPosteArduino=6;
           tornilloArduino=3.2;
*/
           
      translate([-53.3/2,68.6/2,-lonZ_B/2+(espesor/2)])
       rotate(-90)
union(){       
translate([13.6,3,0])
poste(diametroPosteArduino+ajustePoste,tornilloArduino,lonZ_B-espesor/2);

translate([13.6+51.9,3+4.7,0])
poste(diametroPosteArduino+ajustePoste,tornilloArduino,lonZ_B-espesor/2);

translate([13.6+51.9,3+4.7+27.9,0])
poste(diametroPosteArduino+ajustePoste,tornilloArduino,lonZ_B-espesor/2);

translate([13.6+1.1,3+4.7+27.9+15.2,0])
poste(diametroPosteArduino+ajustePoste,tornilloArduino,lonZ_B-espesor/2);
}//fin postes arduino


       }
       
    //recorte Arduino UNO   
      translate([0,0,4.5]) 
       rotate(-90)
      ArduinoUNO(taladroEntradas=20,tol=0.2,ajustePostR=0,modo=1,ajusteTornillos=0.75);
       
       //cubo de correccion de altura
       //A Partir de 4.5 en medida z hay cambio respecto al recorte de tarjeta arduino
       cube([56,70,4.8],center=true);
       
       //recorte repara postes arduino UNO
       
       translate([0,-5,4])
       cube([56,60,10],center=true);
       
       
     //recortador Estrella
       translate([0,-5,-50])
       linear_extrude(height=100)
       //RecortadorEstrella(lonX=53, lonY=30,orillas=1,redondeadorPoly=0.5);
       RecortadorEstrellaInverso(lonX=58,lonY=50,orillas=3,redondeadorPoly=2);
 
  }
}

}//fin modulo case arduino

//#####RENDERIZADOS#####
$fn=20;
//tol=0.5;

//completo


//caseArduino(parte=0,logo=1);
caseArduino(parte=1,logo=2,texto1="Monequi",texto2="",puntos=7.5);



/*
 translate([0,0,4.5]) 
       rotate(-90)
     ArduinoUNO(taladroEntradas=2,tol=0.2,modo=1);
*/

//caseArduino(parte=1);
