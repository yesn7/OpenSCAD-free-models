include<./../CASES/CASEgamma/CASEgamma.scad>;
include<./../Utilidades/Utilities.scad>;
//import("/home/yesn7/Documentos/clases-control-robots-industriales/proyecto/Parametric_pulley_-_lots_of_tooth_profiles/files/T10-10T-OD29.97.stl");
flecha=4;
distanciamotores=90;
espesor=3; //material a imprimir
diametroapoyomotores=48;
alturamotor=72;
module motorNamiki(){
alturamotor=72;
flecha=4;
angulo=50+90; //ayuda a crear los agujeros
translate([0,0,-alturamotor])
cylinder(d=25, h=alturamotor);
//recorte tornillo 1
translate([17,0,-alturamotor/2])
cylinder(d=3.5, h=alturamotor);
//recorte tornillo 2
rotate(angulo)
translate([17,0,-alturamotor/2])
cylinder(d=3.5, h=alturamotor);
//flecha
cylinder(d=flecha, h=17); 
 }
 
 module caja(lonX = 20, lonY = 20, lonZ = 20, espesor=1.5){
     difference(){
    cube(lonX+(2*espesor),lonY+(2*espesor),lonZ*2+(2*espesor),center=true);
         cube(lonX,lonY,lonZ*2,center=true);
         translate([0,0,-lonZ])
         cube(lonX*2,lonY*2,lonZ*2,center=true);
         
         }
     }
      //diametroapoyomotores+distanciamotores
 module eslabon(A=100){
 difference(){
 union(){
 translate([0,-diametroapoyomotores/2*0.5])
 square([A,diametroapoyomotores*0.5]);
 
 circle(d=diametroapoyomotores*0.5);
 
 
 translate([A,0])
 circle(d=diametroapoyomotores*0.5);
     
 }
 translate([A,0])
 circle(d=flecha);
 
 
 
 circle(d=flecha);
 }
 }
 //     RENDERIZADOS
 color("orange")
 difference(){
    CASEgamma(lonX=diametroapoyomotores+distanciamotores,lonY=diametroapoyomotores,medZ=alturamotor,espesor=espesor,diametroPoste=5,tornillo=2.5,parte=0,minkow=3,soporteA=0,soporteB=0,soporteC=0,soporteD=0);
    translate([-distanciamotores/2,0,alturamotor])
union(){
motorNamiki();
 translate([distanciamotores,0,0])
 motorNamiki();
 }
 }
 
  color("pink")
   translate([-distanciamotores/2,0,alturamotor+40])
 rotate([180,0,0])
 import("./files/T10-10T-OD29.97.stl");
 
   color("pink")
   translate([distanciamotores/2,0,alturamotor+20])
 rotate([180,0,0])
 import("./files/T10-10T-OD29.97.stl");
   color("pink")
    translate([distanciamotores/2,0,alturamotor+40])
 rotate([180,0,0])
 import("./files/T10-10T-OD29.97.stl");
   
 A=100;
 color("orange")
 translate([distanciamotores/2, 0, alturamotor])
 linear_extrude(height=espesor)
 eslabon(A=A);
  color("pink")
  translate([distanciamotores/2+A, 0, alturamotor+espesor])
 linear_extrude(height=espesor)
 eslabon(A=A);
  color("pink")
    translate([distanciamotores/2+A,0,alturamotor+20])
 rotate([180,0,0])
 import("./files/T10-10T-OD29.97.stl");
 
