
//home/pablovc/Documentos/OpenSCAD-free-models/SistemaTiDi
include<./../CASEgamma/CASEgamma.scad>;
include<./../Componentes/boards.scad>;
include<./../Componentes/sensores.scad>;
include<./../Utilidades/Utilities.scad>;
include<./../DitacTechnic/Technic.scad>;


//el modulo capsula se desplaza en el eje "x"
module capsula(radio1=2,radio2=3,largo=10){
 hull(){
             
          sphere(r=radio1);
          
          translate([largo,0,0])
          sphere(r=radio2);
          
      }

}

//####RENDERIZADOS####

    
    long=50;
    
   //alfa1=10+20;
   //alfa2=45;
   //alfa3=30+10;
  
 // alfa=(alfa1+alfa2+alfa3)/3;
alfa=90;  


  Perimetro=50;
  Pi=3.14159;
  radioAnclaje=(Perimetro)/(2*Pi);
radioFinal=5; 
 radioIntermedio=(radioAnclaje+radioFinal)/2;
 radRedondeo=3;
  espesor=1.5;

//angulo para broches
co=radioAnclaje-radioIntermedio;
hip=sqrt((long*long)+(co*co));
angBroches=asin(co/hip)*Pi;
  
tol=3;
echo("radioAnclaje");
echo(radioAnclaje);

  
  $fn=100;

//apoyo giratorio
rotate([180,0,0])
translate([0,0,radioAnclaje+tol])
union(){
    
    cylinder(r=6,h=espesor);
    translate([0,0,espesor])
   pinRedondo();
}

difference(){
  
  union(){
    rotate([0,90,0])
    cylinder(r1=radioAnclaje+tol+espesor,r2=radioIntermedio+espesor,h=long);
  
      //codo
   translate([long,0,0])
  sphere(radioIntermedio+espesor);

//tercer seccion
 translate([long,0,0])
  rotate([0,90,alfa])
    cylinder(r1=radioIntermedio+espesor,r2=radioFinal+espesor,h=long);
    
      }
    
    //corte interior
    //40 mm es el largo estimado del muñon
     capsula(radio1=radioAnclaje+tol,radio2=radioIntermedio+1,largo=40);  
      
      /*
      //corte superior
      translate([-radioAnclaje*2,0,0])
    cube([radioAnclaje*4,radioAnclaje*4,radioAnclaje*4],center=true);
      */
      
      //corte mitad
     translate([0,0,(long/2)])
    
      //redondeo
    recortadorMink(lonX=long,lonY=long,lonZ=long,esf=radRedondeo);
      
      
  } 


  
 //Pie
      
   translate([long,0,0])
   rotate(alfa)
   translate([long,0,0])
 
    sphere(r=radioAnclaje+espesor);
      
    
  
  
  //####BROCHES####
  

      
        //broche A
      
  translate([long/4,-radioAnclaje-radioAnclaje/4-radioAnclaje/8,0])
 rotate(angBroches-1)
  
  difference(){
   recortadorMink(lonX=long/2,lonY=radioAnclaje/2,lonZ=radioAnclaje/2,esf=radRedondeo/2);
  
      cube([(long/2)-radRedondeo,radioAnclaje/6,radioAnclaje],center=true);
      }
      
  
              //broche B
       mirror([0,1,0]){
     
   translate([long/4,-radioAnclaje-radioAnclaje/4-radioAnclaje/8,0])
 rotate(angBroches-1)
  
  difference(){
   recortadorMink(lonX=long/2,lonY=radioAnclaje/2,lonZ=radioAnclaje/2,esf=radRedondeo/2);
  
      cube([(long/2)-radRedondeo,radioAnclaje/6,radioAnclaje],center=true);
      }
     
  }
  
  
  //APOYO
  translate([0,0,-radioAnclaje*2-5])
 rotate([0,0,-90])
  !barraMuslo();
  
  
  module barraMuslo(){
      difference(){
        
        translate([(long/2)-4,0,4])
       // cube([50,8,8]);
          difference(){
     recortadorMink(lonX=long,lonY=8,lonZ=7,esf=radRedondeo/2);
          
              
              //primer agujero
              translate([long/5,0,radRedondeo/2])
              rotate([90,0,0])
              cube([(long/2)-radRedondeo,radioAnclaje/6,radioAnclaje*2],center=true);
              
              
              /*
               //segundo agujero
              translate([-long/5,0,0])
              rotate([90,0,0])
              cube([((long/2)-radRedondeo)*(2/3),radioAnclaje/6,radioAnclaje*2],center=true);
              */
              }
        
                         
            agujeroTech();
          
          
           
    }
}
  

  
  
  

  

