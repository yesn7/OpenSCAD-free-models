
poste=5.4;
//tol=1;
nel=false;
simon=true;
alturaConos=1.5;
espesor=.90;



difference(){
//poste sin hueco

union(){
    
    //cono abajo
translate([0,0,-(poste/2)-alturaConos])
cylinder(d1=6,d2=4.05,h=alturaConos);


//cono arriba
mirror([0,0,1]){
translate([0,0,-(poste/2)-alturaConos])
cylinder(d1=6,d2=4.05,h=alturaConos);
}

//poste
cylinder(d=4.05,h=poste,center=simon

);
}

//hueco

cylinder(d=4.05-espesor,h=poste*2,center=simon);

//hueco cono abajo

translate([0,0,-(poste/2)-alturaConos])
cylinder(d1=6-espesor,d2=4.05-espesor,h=alturaConos);

//hueco cono arriba

mirror([0,0,1]){
translate([0,0,-(poste/2)-alturaConos])
cylinder(d1=6-espesor,d2=4.05-espesor,h=alturaConos);
}


}



$fn=100;