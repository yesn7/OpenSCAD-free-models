/*
Radio=30;
pared=10;
grueso=2;
*/

//$fn=8;

module disco(resoluciOn=100,Radio=30,pared=10,grueso=2){
difference(){
    
    
cylinder(r=Radio,center=true,h=grueso,$fn=resoluciOn);
cylinder(r=Radio-pared,center=true,h=grueso*2,$fn=resoluciOn);
       
}

}


//####RENDERIZADOS####
//rotate(360/16)
difference(){
    Radio=30;
    pared=10;
    rotate(360/16)
   disco(resoluciOn=8,Radio=Radio+(pared/6),pared=pared,grueso=2);
   translate([-Radio*1.5,0,-Radio*1.5])
    cube([Radio*3,Radio*3,Radio*3]);
}
    
rotate(180)
difference(){
    Radio=30;
   disco(resoluciOn=100,Radio=Radio-0.75,pared=9.25,grueso=2);
   translate([-Radio*1.5,0,-Radio*1.5])
    cube([Radio*3,Radio*3,Radio*3]);
}
    