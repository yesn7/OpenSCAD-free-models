

include<./../CASEgamma/CASEgamma.scad>;
include<./../Componentes/boards.scad>;
include<./../Componentes/sensores.scad>;
include<./../Utilidades/Utilities.scad>;

module Muebles(escala=100,lonXr=890,lonYr=820,lonZr=480,altoBrazoR=470,altoRespaldoR=950,espesorR=60,brazosSillon=1, respaldoSilla=1,cajon=1,tol=0,cajoneras=1){
   
   lonX=lonXr*(1/escala);
   lonY=lonYr*(1/escala);
   lonZ=lonZr*(1/escala);
    
 respaldo=(1/6)*lonX;
 brazo=respaldo*0.8;
 longBrazo=lonX-respaldo;
 altoBrazo=altoBrazoR*(1/escala);
 altoRespaldo=altoRespaldoR*(1/escala);
    espesor=espesorR*(1/escala);
    
 //base   
if(cajon==0){
       
            cube([lonX,lonY,lonZ]);  
            
     }
 
if(cajon==1){
         difference(){ 
            cube([lonX,lonY,lonZ]);  
            translate([-lonX,espesor,espesor])
            cube([lonX*2,lonY-(2*espesor),lonZ-(2*espesor)]);
         }
         
         //respectivo cajon
         if(cajoneras==1){
         translate([lonX*2,0,0])
      
          difference(){
             cube([lonX-(2*espesor)-(2*tol),lonY-(2*tol),lonZ-(2*espesor)-(2*tol)]);
          translate([espesor,espesor,espesor])
             cube([lonX-(2*tol)-(4*espesor),lonY-(2*espesor)-(2*tol),lonZ*2]);
          }
        }
         
  }
  else if(cajon==2){
          difference(){
              cube([lonX,lonY,lonZ]);
              translate([espesor,-lonY,espesor])
              cube([lonX-(2*espesor),lonY*2,lonZ-(2*espesor)]);
     }
     
     //respectivo cajon
         if(cajoneras==1){
         translate([0,lonY*2,0])
      
          difference(){
             cube([lonX-(2*tol),lonY-(2*espesor)-(2*tol),lonZ-(2*espesor)-(2*tol)]);
          translate([espesor,espesor,espesor])
             cube([lonX-(2*espesor)-(2*tol),lonY-(4*espesor)-(2*tol),lonZ*2]);
          }
        }
        
 }
         
  
  
      
      
  
  
 

  
     if(brazosSillon==1){
 //brazo1
 translate([respaldo,0,lonZ])
 cube([longBrazo,brazo,altoBrazo-(brazo/2)]);
 //detalle redondo
translate([respaldo,brazo/2,lonZ+altoBrazo-(brazo/2)])
rotate([0,90,0])
cylinder(d=brazo,h=longBrazo);


//brazo2
translate([respaldo,lonY-brazo,lonZ])
 cube([longBrazo,brazo,altoBrazo-(brazo/2)]);
 //detalle redondo
 translate([respaldo,-brazo/2+lonY,lonZ+altoBrazo-(brazo/2)])
rotate([0,90,0])
cylinder(d=brazo,h=longBrazo);
     }
 
 
 //respaldo
 if(respaldoSilla==1){
 translate([0,0,lonZ])
 cube([respaldo,lonY,altoRespaldo-(respaldo/2)]);
 
 translate([respaldo/2,0,lonZ+altoRespaldo-(respaldo/2)])
 rotate([0,90,90])
cylinder(d=respaldo,h=lonY);
 }
 
 
 }//fin modulo
 
 //#######RENDERIZADOS########
 
 
  $fn=100;
escala=50;
//sillon sencillo //Muebles(escala=escala,lonXr=890,lonYr=820,lonZr=480,altoBrazoR=470,altoRespaldoR=950,espesorR=90,brazosSillon=1, respaldoSilla=1,cajon=2,tol=0,cajoneras=0);
 
 //sillon sencillo void
//Muebles(escala=escala,lonXr=890,lonYr=820,lonZr=480,altoBrazoR=470,altoRespaldoR=950,espesorR=90,brazosSillon=1, respaldoSilla=1,cajon=0,tol=0,cajoneras=0);
 
 

 
//Sillon Doble
 //Muebles(escala=escala,lonXr=890,lonYr=820*2,lonZr=480,altoBrazoR=470,altoRespaldoR=950,espesorR=90,brazosSillon=1, respaldoSilla=1,cajon=1,tol=0,cajoneras=0);

 //Sillon Doble void
Muebles(escala=escala,lonXr=890,lonYr=820*2,lonZr=480,altoBrazoR=470,altoRespaldoR=950,espesorR=90,brazosSillon=1, respaldoSilla=1,cajon=0,tol=0,cajoneras=0);
 
 
 
 //Silla sencilla
//Muebles(escala=escala,lonXr=890,lonYr=820,lonZr=480,altoBrazoR=470,altoRespaldoR=950,espesorR=90,brazosSillon=0, respaldoSilla=1,cajon=2,tol=0,cajoneras=0);
 
  //Silla sencilla void
//Muebles(escala=escala,lonXr=890,lonYr=820,lonZr=480,altoBrazoR=470,altoRespaldoR=950,espesorR=90,brazosSillon=0, respaldoSilla=1,cajon=0,tol=0,cajoneras=0);
 
 
//Silla Doble 
 //Muebles(escala=escala,lonXr=890,lonYr=820*2,lonZr=480,altoBrazoR=470,altoRespaldoR=950,espesorR=90,brazosSillon=0, respaldoSilla=1,cajon=1,tol=0,cajoneras=0);
 
 //Silla Doble void
 //Muebles(escala=escala,lonXr=890,lonYr=820*2,lonZr=480,altoBrazoR=470,altoRespaldoR=950,espesorR=90,brazosSillon=0, respaldoSilla=1,cajon=0,tol=0,cajoneras=0);
 
 
 //Mesa
//Muebles(escala=escala,lonXr=1000,lonYr=820*3,lonZr=900,altoBrazoR=470,altoRespaldoR=950,espesorR=90,brazosSillon=0, respaldoSilla=0,cajon=1,tol=0,cajoneras=0);
 
  
 //Mesa Void
//Muebles(escala=escala,lonXr=1000,lonYr=820*3,lonZr=900,altoBrazoR=470,altoRespaldoR=950,espesorR=90,brazosSillon=0, respaldoSilla=0,cajon=0,tol=0,cajoneras=0);
 
 