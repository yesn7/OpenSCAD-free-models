//llavero

espesor=1.5;
ajusteFondo=1;

difference(){
    
translate([0,0,espesor/2])
cube([(12*2)+26.6,43.3+4.7+24,5.3+espesor+ajusteFondo],center=true);
    
    //llavero
    union(){
        translate([0,(4.7/2)+(43.3/2),0])
cube([10.5,4.7,5.3+ajusteFondo],center=true);

cube([26.6,43.3,5.3++ajusteFondo],center=true);

    }
    
    //translate([0,-2,0])
    cube([22,27.5+15,40],center=true);
    
    ajuste=19;
    ajusteVer=1.5;
    
    //AGUJERO (1,1)
    translate([(26.6/2)+24-ajuste,(57.8+ajusteVer)/2,0])
    cylinder(d=5.75,h=10,center=true);
    
     //AGUJERO (-1,1)
    translate([-((26.6/2)+24-ajuste),(57.8+ajusteVer)/2,0])
    cylinder(d=5.75,h=10,center=true);
    
     //AGUJERO (1,-1)
    translate([(26.6/2)+24-ajuste,-(57.8+ajusteVer)/2,0])
    cylinder(d=5.75,h=10,center=true);
    
    //AGUJERO (-1,-1)
    translate([-((26.6/2)+24-ajuste),-(57.8+ajusteVer)/2,0])
    cylinder(d=5.75,h=10,center=true);
    
    
}