//LOGOS
include<./../Logos/LogoIdea161.scad>;
include<./../Logos/LogoDitacRegSCorch.scad>;

//include<./../CASEgamma/CASEgamma.scad>;
//include<./../Componentes/boards.scad>;
//include<./../Componentes/sensores.scad>;
include<./../Utilidades/Utilities.scad>;

   

//#####RENDERIZADOS#####

module Base(distLlaves=85,diametro=20,lonMin=0.01){
      //base
    hull(){
        
        translate([-distLlaves/2,0,0])
        /*difference(){
        sphere(d=diametro);
        translate([0,0,-diametro/2])
            cube([diametro,diametro,diametro],center=true);
        }*/
        cylinder(d=diametro,h=lonMin);

        translate([distLlaves/2,0,0])
        /*difference(){
        sphere(d=diametro);
        translate([0,0,-diametro/2])
            cube([diametro,diametro,diametro],center=true);
        }*/
        cylinder(d=diametro,h=lonMin);

    }
}

module topeLlave(altura=2){
    difference(){
    
   cylinder(d=16,h=2,center=true);
    cylinder(d=6.5,h=4,center=true);
   
}
    
}

module Llavero(alturaTapas=9.5,logo=1,texto1="Pablo VC",texto2="IDEA 1.61"){

distLlaves=85;
diametro=20;
    diamMenor=14;
    diamTornillo=9;
    tol=1;
    diamTuerca=11.5;
    tol=2;
espesor=1.5;
    
    //alturaTapas=9.5;
   altTornillo=4;
    altTuerca=6;
    

redonMink=1;
lonX=distLlaves+diametro;
lonY=diametro;
lonZ=espesor*2;
    lonMin=0.01;

difference(){

union(){

//tapa tornillo
translate([0,0,alturaTapas/2])
difference(){
    
   //resize([distLlaves+diametro,diametro,espesor])
    hull(){
  Base(distLlaves=distLlaves,diametro=diametro,lonMin=lonMin);
    
    translate([0,0,altTornillo+espesor])
    Base(distLlaves=distLlaves,diametro=diamMenor,lonMin=lonMin);
    }
    //agujero 1
      translate([-distLlaves/2,0,0])
    cylinder(d=6.5,h=espesor*2,center=true);
    //agujero 1 (tornillo)
    translate([-distLlaves/2,0,espesor])
    cylinder(d=diamTornillo+tol,h=altTornillo*2);
    
    //agujero 2
    //cylinder(d=6.5,h=espesor*2,center=true);
    
      //agujero 3
     translate([distLlaves/2,0,0])
    cylinder(d=6.5,h=espesor*2,center=true);
     //agujero 3 (tornillo)
    translate([distLlaves/2,0,espesor])
    cylinder(d=diamTornillo+tol,h=altTornillo*2);
    
}

rotate(9)
cube([distLlaves-(diametro*2)-(espesor*4),espesor*2,alturaTapas+espesor*2],center=true);

//tapa tuerca
rotate([180,0,0])
translate([0,0,alturaTapas/2])
difference(){
    
   //resize([distLlaves+diametro,diametro,espesor])
    hull(){
  Base(distLlaves=distLlaves,diametro=diametro,lonMin=lonMin);
    
    translate([0,0,altTornillo+espesor])
    Base(distLlaves=distLlaves,diametro=diamMenor,lonMin=lonMin);
    }
    //agujero 1
      translate([-distLlaves/2,0,0])
    cylinder(d=6.5,h=espesor*2,center=true);
    //agujero 1 (tuerca)
    translate([-distLlaves/2,0,espesor])
      rotate(90)
    cylinder(d=diamTuerca,h=altTornillo*2,$fn=6);
    
   
      //agujero 2
     translate([distLlaves/2,0,0])
    cylinder(d=6.5,h=espesor*2,center=true);
     //agujero 2 (tuerca)
    translate([distLlaves/2,0,espesor])
    rotate(90)
    cylinder(d=diamTuerca,h=altTornillo*2,$fn=6);
    
}

}//fin union

//recortador dedo
/*
translate([-distLlaves/4,diametro-((diametro-diamMenor)/2),0])
cylinder(d=diametro,h=alturaTapas*3);

translate([-distLlaves/4-(diametro/4),diametro-((diametro-diamMenor)/2),0])
cylinder(d=diametro,h=alturaTapas*3);

translate([-distLlaves/4-(diametro/2),diametro-((diametro-diamMenor)/2),0])
cylinder(d=diametro,h=alturaTapas*3);
*/

}//fin diffrence NAIL

//LOGO
/*
//logo de Idea
 translate([0,0,(alturaTapas/2)+altTornillo])
rotate(90)
 linear_extrude(height=espesor*1.5)
        Idea(0.16);
*/

if(logo==1){
translate([0,0.5,(alturaTapas/2)+altTornillo+espesor/2])
rotate(90)
//resize([diamMenor,diamMenor/,espesor*1.5])
scale([1,1,2])
//import("/home/pablovc/Descargas/skulltrooper(1).stl");
import("/home/pablovc/Descargas/leosigno.stl");
}


//texto1

tamaNio=6.5;

rotate([180,0,0])
translate([0,0,(alturaTapas/2)+altTornillo])
linear_extrude(height=espesor*1.5)
 text(size=tamaNio,font = "Simplex:style=Bold",texto1,halign ="center",valign="center");

rotate([180,180,0])
translate([0,0,(alturaTapas/2)+altTornillo])
linear_extrude(height=espesor*1.5)
 text(size=tamaNio,font = "Simplex:style=Bold",texto2,halign ="center",valign="center");

//text(size=10,font = "Accanthis ADF Std No3:style=Bold",texto,halign ="center",valign="center");


}//fin llavero

//RENDERIZADOS

$fn=100;
/*projection(){
TapaLlavero();
}*/


//9.5
//rotate([90,0,0])

//####CALCULO DE LLAVES####
//9.5-> 4 espacios (8 llaves)
//2 espacio es 2.375

//Llavero(alturaTapas=9.5,logo=1,texto1="Airam",texto2="");
Llavero(alturaTapas=9.5,logo=0,texto1="nunca dejes",texto2="de soñar");
//2 predeterminado
//"extra " 3
//topeLlave(altura=2);



