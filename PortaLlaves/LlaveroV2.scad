
//LOGOS
include<./../Logos/LogoIdea161.scad>;
include<./../Logos/LogoDitacRegSCorch.scad>;

//include<./../CASEgamma/CASEgamma.scad>;
//include<./../Componentes/boards.scad>;
//include<./../Componentes/sensores.scad>;
include<./../Utilidades/Utilities.scad>;

   

//#####RENDERIZADOS#####
espesor=1.5;
diametro=6.38;
alturaPoste=17;
topeLlave=13;
tol=1;
diametroLlave=26;
largoLlave=73;
anchoDientes=9;

lonX=diametroLlave+(2*espesor);
//lonY=largoLlave+topeLlave+(2*espesor);
lonY_A=(topeLlave*3)+(2*espesor);
lonY_B=largoLlave-(topeLlave*2);
lonZ=alturaPoste+(2*espesor);
redonMink=2;

//poste
difference(){
    union(){
        cylinder(d=diametro-tol,h=alturaPoste+espesor);

        translate([0,0,alturaPoste])
        cylinder(d=diametro-(tol/2),h=espesor);
    }
    translate([-diametro,-diametro/8,espesor+(alturaPoste*0.75)])
  cube([diametro*2,diametro/4,(alturaPoste*2+espesor)/2]);
}


difference(){
    union(){

        //parte Ancha
        
          translate([0,lonY_A/2-espesor-topeLlave,lonZ/2-espesor])
          recortadorMink(lonX=lonX,lonY=lonY_A,lonZ=lonZ,esf=redonMink);
        
         //parte angosta
        translate([0,lonY_B/2+(topeLlave*2),lonZ/2-espesor])
          recortadorMink(lonX=anchoDientes+(2*espesor),lonY=lonY_B+espesor,lonZ=lonZ,esf=redonMink);
        
        
    }//fin union
       // difference(){
        

          translate([-(lonX-(2*espesor))/2,-topeLlave,0])
          cube([lonX*2-(2*espesor),lonY_A-(2*espesor),lonZ*2-(2*espesor)]);
        //}

        //parte angosta
        //translate([0,lonY_B/4+(topeLlave*2),lonZ/2-espesor])
    //translate([-anchoDientes/2,-lonY_B*1.5,-lonZ/2+espesor])
      //    cube([anchoDientes,lonY_B/2,lonZ*2-(2*espesor)]);
        //difference(){
          
          //recortadorMink(lonX=anchoDientes+(2*espesor),lonY=lonY_B+espesor,lonZ=lonZ,esf=redonMink);

         // translate([-anchoDientes/2,-lonY_B*1.5,-lonZ/2+espesor])
          //cube([anchoDientes,lonY_B*2,lonZ*2-(2*espesor)]);
        //}


   // }//fin union

translate([-anchoDientes/2,-topeLlave*1.5+(espesor*4.5),0])
cube([lonX,lonY_A+lonY_B-(espesor*2.5),lonZ]);

}//fin difference

//LOGO

translate([0,lonZ/2,(lonZ-(2*espesor))/2])
rotate([0,-90,0])
translate([0,0,lonX/2-(espesor/2)])

 linear_extrude(height=espesor)
        Idea(0.16);

$fn=100;
/*

//logos
tamaNioLogos=0.5;
//postes arduino
logos=0;
//aparecen logos o texto

if(logos==1){
    translate([-20*tamaNioLogos+2,0,0])
    union(){
       linear_extrude(height=espesor/2)
       Ditac(tamaNioLogos);


        translate([30*tamaNioLogos,5*tamaNioLogos])
        linear_extrude(height=espesor/2)
        Idea(tamaNioLogos/4);
    }
}
else if(logos==0){
translate([-19,-9,0])
linear_extrude(height=espesor/2)    
text(size=12,font = "Simplex","Vero");

}


$fn=100;

difference(){
     translate([0,0,-espesor])
        cylinder(r=20,h=espesor);
    translate([0,10,-espesor*2])
    cylinder(r=2,h=4*espesor);
       
}

*/