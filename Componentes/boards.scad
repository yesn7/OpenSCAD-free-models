//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!***************************
// Nota: Aún no está completo, pero tiene las dimeniones correctas y los tornillos.
//Falta agregar pines
// L298D agregado sin detallar
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!***************************

module PlacaConectaDraft(taladroEntradas=10,tol=0.2,ajustePostR=1,ajusteTornillo=0){
      /*
    medidas 
    59.69
    61.595
    12.5
    */
       //medidas
 
    //medidas
    espesorArduino=1.6;

//tarjeta
cube([59.69,61.595,espesorArduino]);

//entrada DC
//translate([-taladroEntradas,3.3-tol,espesorArduino])
//cube([taladroEntradas,8.9+(2*tol),10.9+(2*tol)]);

//entrada USB
//translate([-taladroEntradas,31.7,espesorArduino])
//cube([taladroEntradas,12+(2*tol),10.9+(2*tol)]);

//Led RX TX
    //se tomo la coordenada de los pines analogicos mas 30 en el eje "Y"
//translate([13.6+12.5-tol,2+30-tol,espesorArduino])
//cube([2+(2*tol),4+(2*tol),10.9+taladroEntradas]);

//Led RX TX
    //se tomo la coordenada de los LED RX TX más 4mm (+4 por la medida de las ventanas) en el eje "Y"
//translate([13.6+12.5-tol,2+30+8-tol,espesorArduino])
//cube([2+(2*tol),2+(2*tol),10.9+taladroEntradas]);

//ICSP
    //se tomo la coordenada de los pines analogicos mas 30 en el eje "Y"
//translate([13.6+12.5-tol,2+30-tol,espesorArduino])
/*
espesor=1.5;
translate([14,44.5,espesorArduino])
cube([7.5+(2*tol),5+(2*tol),10.9+taladroEntradas-(espesor*6)]);
*/


//entrada pines (analogicos)


//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!***************************
// ESTO ESTÁ PENDIENTE PARA UN CASEEEEEEEEEEEEEEEEEEEE
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!***************************


//translate([13.6+12.5-tol,1-tol,espesorArduino])
//cube([33+5.5+(2*tol),2+(2*tol),10.9+taladroEntradas]);

//entrada pines (digitales + PWM)
//translate([13.6+2.5-tol+ajustePostR,53.3-2-1-tol,espesorArduino])
//cube([48+(2*tol)-ajustePostR,2+(2*tol),10.9+taladroEntradas]);

//###Tornillos ##### Tornillo inferior izquierdo
translate([1.905,9.525,0])
rotate([180,0,0])
cylinder($fn=100,d=3.2+ajusteTornillos,h=taladroEntradas);

//##### Tornillo inferior derecho
translate([1.905+55.88,9.525,0])
rotate([180,0,0])
cylinder(d=3.2+ajusteTornillos,h=taladroEntradas);

//##### Tornillo superior derecho
translate([2.54+54.610,9.525+49.530,0])
rotate([180,0,0])
cylinder(d=3.2+ajusteTornillos,h=taladroEntradas);

//##### Tornillo superior izquierdo
translate([2.54,9.525+49.530,0])
rotate([180,0,0])
cylinder(d=3.2+ajusteTornillos,h=taladroEntradas);

//boton arduino
 //se redujo 8 mm en eje "x" y se aumento 1mm en eje "y" del la coordenada del último tornillo
//      translate([13.6+1.1-8,3+4.7+27.9+15.2-0.5,0])
//       cylinder(d=3.2,h=10.9+taladroEntradas);
   


}//fin module arduino uno draft


module L298NDraft(taladroEntradas=10,tol=0.2,ajustePostR=1,ajusteTornillo=0){
      /*
    medidas L298N
    43
    43
    12.5
    */
       //medidas
 
    //medidas
    espesorArduino=1.6;

//tarjeta
cube([43,43,espesorArduino]);

//entrada DC
//translate([-taladroEntradas,3.3-tol,espesorArduino])
//cube([taladroEntradas,8.9+(2*tol),10.9+(2*tol)]);

//entrada USB
//translate([-taladroEntradas,31.7,espesorArduino])
//cube([taladroEntradas,12+(2*tol),10.9+(2*tol)]);

//Led RX TX
    //se tomo la coordenada de los pines analogicos mas 30 en el eje "Y"
//translate([13.6+12.5-tol,2+30-tol,espesorArduino])
//cube([2+(2*tol),4+(2*tol),10.9+taladroEntradas]);

//Led RX TX
    //se tomo la coordenada de los LED RX TX más 4mm (+4 por la medida de las ventanas) en el eje "Y"
//translate([13.6+12.5-tol,2+30+8-tol,espesorArduino])
//cube([2+(2*tol),2+(2*tol),10.9+taladroEntradas]);

//ICSP
    //se tomo la coordenada de los pines analogicos mas 30 en el eje "Y"
//translate([13.6+12.5-tol,2+30-tol,espesorArduino])
/*
espesor=1.5;
translate([14,44.5,espesorArduino])
cube([7.5+(2*tol),5+(2*tol),10.9+taladroEntradas-(espesor*6)]);
*/


//entrada pines (analogicos)


//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!***************************
// ESTO ESTÁ PENDIENTE PARA UN CASEEEEEEEEEEEEEEEEEEEE
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!***************************


//translate([13.6+12.5-tol,1-tol,espesorArduino])
//cube([33+5.5+(2*tol),2+(2*tol),10.9+taladroEntradas]);

//entrada pines (digitales + PWM)
//translate([13.6+2.5-tol+ajustePostR,53.3-2-1-tol,espesorArduino])
//cube([48+(2*tol)-ajustePostR,2+(2*tol),10.9+taladroEntradas]);

//###Tornillos arduino##### Tornillo inferior izquierdo
translate([3,3,0])
rotate([180,0,0])
cylinder($fn=100,d=3.2+ajusteTornillos,h=taladroEntradas);

//##### Tornillo inferior derecho
translate([37+3,3,0])
rotate([180,0,0])
cylinder(d=3.2+ajusteTornillos,h=taladroEntradas);

//##### Tornillo superior derecho
translate([3+37,3+37,0])
rotate([180,0,0])
cylinder(d=3.2+ajusteTornillos,h=taladroEntradas);

//##### Tornillo superior izquierdo
translate([3,3+37,0])
rotate([180,0,0])
cylinder(d=3.2+ajusteTornillos,h=taladroEntradas);

//boton arduino
 //se redujo 8 mm en eje "x" y se aumento 1mm en eje "y" del la coordenada del último tornillo
//      translate([13.6+1.1-8,3+4.7+27.9+15.2-0.5,0])
//       cylinder(d=3.2,h=10.9+taladroEntradas);
   


}//fin module arduino uno draft

module ArduinoMEGADraft(taladroEntradas=10,tol=0.2,ajustePostR=1,ajusteTornillo=0){
      /*
    medidas ARDUINO MEGA
    101.6
    53.3
    12.5
    */
       //medidas
 
    //medidas
    espesorArduino=1.6;

//tarjeta
cube([101.6,53.3,espesorArduino]);

//entrada DC
translate([-taladroEntradas,3.3-tol,espesorArduino])
cube([taladroEntradas,8.9+(2*tol),10.9+(2*tol)]);

//entrada USB
translate([-taladroEntradas,31.7,espesorArduino])
cube([taladroEntradas,12+(2*tol),10.9+(2*tol)]);

//Led RX TX
    //se tomo la coordenada de los pines analogicos mas 30 en el eje "Y"
translate([13.6+12.5-tol,2+30-tol,espesorArduino])
cube([2+(2*tol),4+(2*tol),10.9+taladroEntradas]);

//Led RX TX
    //se tomo la coordenada de los LED RX TX más 4mm (+4 por la medida de las ventanas) en el eje "Y"
translate([13.6+12.5-tol,2+30+8-tol,espesorArduino])
cube([2+(2*tol),2+(2*tol),10.9+taladroEntradas]);

//ICSP
    //se tomo la coordenada de los pines analogicos mas 30 en el eje "Y"
//translate([13.6+12.5-tol,2+30-tol,espesorArduino])
/*
espesor=1.5;
translate([14,44.5,espesorArduino])
cube([7.5+(2*tol),5+(2*tol),10.9+taladroEntradas-(espesor*6)]);
*/


//entrada pines (analogicos)


//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!***************************
// ESTO ESTÁ PENDIENTE PARA UN CASEEEEEEEEEEEEEEEEEEEE
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!***************************


translate([13.6+12.5-tol,1-tol,espesorArduino])
cube([33+5.5+(2*tol),2+(2*tol),10.9+taladroEntradas]);

//entrada pines (digitales + PWM)
translate([13.6+2.5-tol+ajustePostR,53.3-2-1-tol,espesorArduino])
cube([48+(2*tol)-ajustePostR,2+(2*tol),10.9+taladroEntradas]);

//###Tornillos arduino##### Tornillo inferior izquierdo
translate([15.24,3,0])
rotate([180,0,0])
cylinder($fn=100,d=3.2+ajusteTornillos,h=taladroEntradas);

//##### Tornillo inferior derecho
translate([15.24+81.28,0])
rotate([180,0,0])
cylinder(d=3.2+ajusteTornillos,h=taladroEntradas);

//##### Tornillo superior derecho
translate([15.24+74.93,3+4.7+27.9+15.2,0])
rotate([180,0,0])
cylinder(d=3.2+ajusteTornillos,h=taladroEntradas);

//##### Tornillo superior izquierdo
translate([15.24,3+4.7+27.9+15.2,0])
rotate([180,0,0])
cylinder(d=3.2+ajusteTornillos,h=taladroEntradas);

//boton arduino
 //se redujo 8 mm en eje "x" y se aumento 1mm en eje "y" del la coordenada del último tornillo
      translate([13.6+1.1-8,3+4.7+27.9+15.2-0.5,0])
       cylinder(d=3.2,h=10.9+taladroEntradas);
   


}//fin module arduino uno draft
module ArduinoUNODraft(taladroEntradas=10,tol=0.2,ajustePostR=1,ajusteTornillos=0){
    
     /*
    medidas ARDUINO UNO
    68.6
    53.3
    12.5
    */
    
    //medidas
    espesorArduino=1.6;

//tarjeta
cube([68.6,53.3,espesorArduino]);

//entrada DC
translate([-taladroEntradas,3.3-tol,espesorArduino])
cube([taladroEntradas,8.9+(2*tol),10.9+(2*tol)]);

//entrada USB
translate([-taladroEntradas,31.7,espesorArduino])
cube([taladroEntradas,12+(2*tol),10.9+(2*tol)]);

//Led RX TX
    //se tomo la coordenada de los pines analogicos mas 30 en el eje "Y"
translate([13.6+12.5-tol,2+30-tol,espesorArduino])
cube([2+(2*tol),4+(2*tol),10.9+taladroEntradas]);

//Led RX TX
    //se tomo la coordenada de los LED RX TX más 4mm (+4 por la medida de las ventanas) en el eje "Y"
translate([13.6+12.5-tol,2+30+8-tol,espesorArduino])
cube([2+(2*tol),2+(2*tol),10.9+taladroEntradas]);

//ICSP
    //se tomo la coordenada de los pines analogicos mas 30 en el eje "Y"
//translate([13.6+12.5-tol,2+30-tol,espesorArduino])
/*
espesor=1.5;
translate([14,44.5,espesorArduino])
cube([7.5+(2*tol),5+(2*tol),10.9+taladroEntradas-(espesor*6)]);
*/


//entrada pines (analogicos)
translate([13.6+12.5-tol,1-tol,espesorArduino])
cube([33+5.5+(2*tol),2+(2*tol),10.9+taladroEntradas]);

//entrada pines (digitales + PWM)
translate([13.6+2.5-tol+ajustePostR,53.3-2-1-tol,espesorArduino])
cube([48+(2*tol)-ajustePostR,2+(2*tol),10.9+taladroEntradas]);

//###Tornillos arduino#####
translate([13.6,3,0])
rotate([180,0,0])
cylinder($fn=100,d=3.2+ajusteTornillos,h=taladroEntradas);


translate([13.6+51.9,3+4.7,0])
rotate([180,0,0])
cylinder(d=3.2+ajusteTornillos,h=taladroEntradas);

translate([13.6+51.9,3+4.7+27.9,0])
rotate([180,0,0])
cylinder(d=3.2+ajusteTornillos,h=taladroEntradas);

translate([13.6+1.1,3+4.7+27.9+15.2,0])
rotate([180,0,0])
cylinder(d=3.2+ajusteTornillos,h=taladroEntradas);

//boton arduino
 //se redujo 8 mm en eje "x" y se aumento 1mm en eje "y" del la coordenada del último tornillo
      translate([13.6+1.1-8,3+4.7+27.9+15.2-0.5,0])
       cylinder(d=3.2,h=10.9+taladroEntradas);
   


}//fin module arduino uno draft

module ArduinoUNO(taladroEntradas=10,tol=0.2,ajustePostR=1,modo=0,ajusteTornillos=0){
   
   /*
    medidas ARDUINO UNO
    68.6
    53.3
    12.5
    */
    
    echo("ArduinoUNO x y z",68.6,53.3,12.5);
   
    if(modo==0){

            ArduinoUNODraft(taladroEntradas=taladroEntradas,tol=tol,ajustePostR=ajustePostR,ajusteTornillos=ajusteTornillos);
    }

     if(modo==1){
            translate([-68.6/2,-53.3/2,-12.5/2])
            ArduinoUNODraft(taladroEntradas=taladroEntradas,tol=tol,ajustePostR=ajustePostR,ajusteTornillos=ajusteTornillos);
    }

}


module RaspberryPiDraft(taladroEntradas=10){
//Raspberry pi
    espesorTarjeta=1.6;
   
    medidaX=85;
    medidaY=56;
    
    
    echo("Raspberry Pi",85,56,1.6);
   
     /*
    medidas Rasberry Pi
     eje "x"= 85mm
     eje "y"= 56mm
      espesorTarjeta=1.6
    */
    
//tarjeta
cube([medidaX,medidaY,espesorTarjeta]);

//Conjunto USB
translate([medidaX,29-(14.3/2),espesorTarjeta])
cube([taladroEntradas,14.3,16]);

translate([medidaX,47-(14.3/2),espesorTarjeta])
cube([taladroEntradas,14.3,16]);

//Conector RJ45
translate([medidaX,10.25-(15.1/2),espesorTarjeta])
cube([taladroEntradas,15.1,13.5]);

//GPIO
translate([3.5*2,49,espesorTarjeta])
cube([52,6,8.5+taladroEntradas]);


//###TornillosRaspberry#####

translate([3.5,3.5,0])
rotate([180,0,0])
cylinder(d=2.75,h=taladroEntradas);


translate([3.5+58,3.5,0])
rotate([180,0,0])
cylinder(d=2.75,h=taladroEntradas);

translate([3.5,3.5+49,0])
rotate([180,0,0])
cylinder(d=2.75,h=taladroEntradas);


translate([3.5+58,3.5+49,0])
rotate([180,0,0])
cylinder(d=2.75,h=taladroEntradas);

}//fin raspberry Pi


module RaspberryPi(taladroEntradas=10,modo=0){
    espesorTarjeta=1.6;
   
    medidaX=85;
    medidaY=56;
    if(modo==0){
    RaspberryPiDraft(taladroEntradas=taladroEntradas);
    }
    else if(modo==1){
        translate([-medidaX/2,-medidaY/2,0])
       RaspberryPiDraft(taladroEntradas=taladroEntradas); 
    }
    
}

//#####RENDERIZADOS####
PlacaConectaDraft();
//L298NDraft();
//ArduinoMEGADraft();
//Se recomienda no tener renderizados (debugging)
//$fn=20;
//los modos se usan para reposicionar el objeto
/*
el primer argumento es la longitud de los taladros
el segundo argumento es el modo o posicionamiento
modo = 0 esquinado
modo = 1 el centro del objeto en el origen
*/

//ArduinoUNO(taladroEntradas=5,tol=0.2,modo=0);

 //RaspberryPi(taladroEntradas=10,modo=1);