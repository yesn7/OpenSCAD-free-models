module SemiCirculo(radio=10,
espesor=3){
    difference(){
        
        
        circle(r=radio);
        circle(r=radio-espesor);
        
        translate([0,-radio])
        square([2*radio,2*radio]);
    }

}

//SemiCirculo(radio=20,espesor=3);

module MonequiLogo(tamanio=10){
    
    //union(){
medida=tamanio/3;
translate([-tamanio,0])
    rotate(180)
//mirror([1,0]){
SemiCirculo(radio=tamanio,espesor=medida);
//}

translate([tamanio/2,0])
SemiCirculo(radio=tamanio/2,espesor=medida/2);


translate([-tamanio,tamanio-(medida/2)])
circle(r=medida);

translate([-tamanio,-tamanio+(medida/2)])
circle(r=medida);


translate([tamanio/2,tamanio/2-(medida/4)])
circle(r=medida/2);

translate([tamanio/2,-tamanio/2+(medida/4)])
circle(r=medida/2);

//}//fin union
}//fin logo monequi

//####RENDERIZADOS###

//$fn=100;

//MonequiLogo(radio=10);
